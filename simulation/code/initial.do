program define initial

set more off
/*

this program creates a file containing an initially stable population
corresponding to a given E(0) and r. 

1. the initial stable population is calculated using CD procedures; to do
this we need two inputs : E(0) and r. The value of E(0) is converted
into the value of E(10) needed to generate a set of Lx's for a life
table with the input E(0). We then use those Lx values and the value
of r to evaluate the stable population expression for Cx


2. The single year fertility function is determined using cubic function
on the Fx values used by CD. The cubic function corresponds to Brass
suggested functional form. See introduction to CD_fertility.dct for
a fulleer expolanation. 

Syntax is as follows:

			initial E(0) r nameoffile

where

	E(0) is value of E01900 (life expectancy in year 1900)
	r is the desired rate of growth
	nameoffile is the name of the file with the CD values of 5Fx (CD_fertility.dta)

*/

/*produces values of Lx to be used in calculations of stable population */
			
/*
transforms inputs for E(0) into values of E(10) to use as inputs for CD tables
see ADJUST.DO for more information on the equation
*/

local E0=`1'		/*value of E(0)*/
local rate=`2'		/*rate of increase*/

local E10=-18.0531 + 2.557*`E0'-.0314*`E0'^2+.000147*`E0'^3


/*calculates the life table and outputs a file with age  Lx, lx and srx  values: 0,1,...100*/

CDtables `E10' CD_coefficients 1 	/*for only one year there will be a file called TABLE_1.DTA*/

/*calculates stable population using simple approximation up to age 79*/



gen Px1900=0
replace Px1900= exp(-`rate'*(.5*(age+age+1))) * (Lx/100000) if _n<=80


/*calculate Gompertz parameters using CD expression and the file produced by CDtables.do*/

local mu105=.613+1.75*Q[20]			/*refers to the value 5Q75 in Table_1.dta*/
local mu775=(lx[76]-lx[81])/Lx[20]		/*refers to l75,l80; Lx[1] refers to 5L75 in Table_1.dta*/
local gompertz= ln(`mu105'/`mu775')/27.5
local mu80= `mu775'*exp(2.5*`gompertz')

/*uses numerical intergration to calculate population above age 80*/

/*first for population between 80 and 100 exactly*/

local x=81
while `x'<=100{	/*note that last calculation will be for age group 99-100*/ 
local d=0
local suma=0
while `d'<=9 {
local dd=`d'/10
local ddd=(`d'+1)/10
local a=exp(-`rate'*.5*(`x'+`dd'+`x'+`ddd'))
local b=(lx[81]/100000)*exp((-`mu80'/`gompertz')*(exp(`gompertz'*(`x'+`ddd'-80))-1))
local bb=(lx[81]/100000)*exp((-`mu80'/`gompertz')*(exp(`gompertz'*(`x'+`dd'-80))-1))
local bbb =.10*((`bb'+`b')/2)		/*each interval is of width .10 of a year*/ 
local suma=`suma'+`a'*`bbb'
local d=`d'+1
}

replace Px1900=`suma' if _n==`x'
local x=`x'+1
}

/*now for population 100 we repeat using expression up until age 150+*/

local sumarum=0
local x=100
while `x'<=150 {
local d=0
local suma=0
while `d'<=9 {
local dd=`d'/10
local ddd=(`d'+1)/10
local a=exp(-`rate'*.5*(`x'+`dd'+`x'+`ddd'))
local b=exp((-`mu80'/`gompertz')*(exp(`gompertz'*(`x'+`ddd'-80))-1))
local bb=exp((-`mu80'/`gompertz')*(exp(`gompertz'*(`x'+`dd'-80))-1))
local bbb=.5*(`bb'+`b')
local suma=`suma'+`a'*`bbb'
local d=`d'+1
}
local sumarum=`sumarum'+`suma' 
local x=`x'+1
}

replace Px1900=`sumarum' if _n==101

/*calculates proportions*/

summarize Px1900
local k=r(mean)*r(N)
replace Px1900=Px1900/`k'
sort age
save temp_pop.dta, replace

drop _all
 
 /*incorporates the values of Fx*/

infile using `3'
rename Fx Fx1900
replace age=age-.5
order age Fx1900
sort age
save temp_fert.dta, replace
drop _all

use temp_pop
merge age using temp_fert
keep  age Px1900 Fx1900
sort age
save initial, replace

end
