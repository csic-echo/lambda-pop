clear
drop _all
program drop _all

program define reg2_thetas
set more off

/*
/****************************/
/** Case 1 : teta1 > teta3 **/
/****************************/
use CLEANsimmatrix_ST

drop if age<45 & age>75		/* eliminates negative cumsr_ values */ 	

keep if (teta3 < teta1) & (comp1==1 & comp2==1 & comp3==1) & cumsr>0
gen icumsr=1/cumsr 

gen change= (ctry!=ctry[_n-1])
gen run=sum(change)
drop change ctry

keep run age cumsr icumsr teta1 teta3 
order run age cumsr icumsr teta1 teta3 

/*it merges each simulated population (under a combination of teta1 and teta3) with
the estimated coefficients of beta in the equation cumsr(x)= alpha + beta1* teta1 + beta3* teta3 */
  
sort age
merge age using coeffC1_betas
sort run age

keep age run cumsr icumsr teta1 teta3 alpha beta1 beta3 rsquare
order age run cumsr icumsr teta1 teta3 alpha beta1 beta3 rsquare

/*this part creates a file with regression coefficents from the expression 

    cumsr(x) - alpha= r1 beta1 + r3 beta3

where r1hat is the estimate of teta1_ and r3hat is the estimate of teta3_ */
 
gen nicumsr= icumsr - alpha     
gen coeff_r1=.
gen coeff_r3=.
gen nrsquare=.

local i=1 
while `i'<=105 /*21*/ {   

reg nicumsr beta1 beta3 if run==`i', noc 
matrix beta_`i'=e(b)
svmat beta_`i'
local nrsquare_`i'=e(r2)

replace coeff_r1=beta_`i'[1,1] if run==`i'
replace coeff_r3=beta_`i'[1,2] if run==`i'
replace nrsquare=e(r2) if run==`i'

drop beta_`i'1 beta_`i'2  

local i=`i'+1
}

rename coeff_r1 nteta1
rename coeff_r3 nteta3

collapse (max) teta1 teta3 nteta1 nteta3 nrsquare, by(run)
format nteta1 - nrsquare %6.4f
save coeffC1_thetas, replace


/****************************/
/** Case 2 : teta3 > teta1 **/
/****************************/
clear
use CLEANsimmatrix_ST  

drop if age<45 | age>75		/* eliminates negative cumsr_ values */ 	

keep if (teta3 > teta1) & (comp1==1 & comp2==1 & comp3==1) & cumsr>0
gen icumsr=1/cumsr 


gen change= (ctry!=ctry[_n-1])
gen run=sum(change)
drop change ctry

keep run age cumsr icumsr teta1 teta3 
order run age cumsr icumsr teta1 teta3 

/*it merges each simulated population (under a combination of teta1 and teta3) with
the estimated coefficients of beta in the equation cumsr(x)= alpha + beta1* teta1 + beta3* teta3 */
  
sort age
merge age using coeffC2_betas
sort run age

keep age run cumsr icumsr teta1 teta3 alpha beta1 beta3 rsquare
order age run cumsr icumsr teta1 teta3 alpha beta1 beta3 rsquare

/*this part creates a file with regression coefficents from the expression 

    cumsr(x) - alpha= r1 beta1 + r3 beta3

where r1hat is the estimate of teta1_ and r3hat is the estimate of teta3_ */
 
gen nicumsr= icumsr - alpha     
gen coeff_r1=.
gen coeff_r3=.
gen nrsquare=.

local i=1 
while `i'<=105 { 

reg nicumsr beta1 beta3 if run==`i', noc 
matrix beta_`i'=e(b)
svmat beta_`i'
local nrsquare_`i'=e(r2)

replace coeff_r1=beta_`i'[1,1] if run==`i'
replace coeff_r3=beta_`i'[1,2] if run==`i'
replace nrsquare=e(r2) if run==`i'

drop beta_`i'1 beta_`i'2  

local i=`i'+1
}

rename coeff_r1 nteta1
rename coeff_r3 nteta3

collapse (max) teta1 teta3 nteta1 nteta3 nrsquare, by(run)
format nteta1 - nrsquare %6.4f
save coeffC2_thetas, replace


/****************************/
/** Case 3 : teta3 = teta1 **/
/****************************/
clear
use CLEANsimmatrix_ST  

drop if age<45 | age>75		/* eliminates negative cumsr_ values */ 	

keep if (teta3 == teta1) & (comp1==1 & comp2==1 & comp3==1) & cumsr>0
gen icumsr=1/cumsr 

gen change= (ctry!=ctry[_n-1])
gen run=sum(change)
drop change ctry

keep run age cumsr icumsr teta1 teta3 
order run age cumsr icumsr teta1 teta3 

/*it merges each simulated population (under a combination of teta1 and teta3) with
the estimated coefficients of beta in the equation cumsr(x)= alpha + beta1* teta1 + beta3* teta3 */
  
sort age
merge age using coeffC3_betas
sort run age

keep age run cumsr icumsr teta1 teta3 alpha beta1 beta3 rsquare
order age run cumsr icumsr teta1 teta3 alpha beta1 beta3 rsquare

/*this part creates a file with regression coefficents from the expression 

    cumsr(x) - alpha= r1 beta1 + r3 beta3

where r1hat is the estimate of teta1_ and r3hat is the estimate of teta3_ */
 
gen nicumsr= icumsr - alpha     
gen coeff_r1=.
gen coeff_r3=.
gen nrsquare=.

local i=1 
while `i'<=15 { 

reg nicumsr beta1 beta3 if run==`i', noc 
matrix beta_`i'=e(b)
svmat beta_`i'
local nrsquare_`i'=e(r2)

replace coeff_r1=beta_`i'[1,1] if run==`i'
replace coeff_r3=beta_`i'[1,2] if run==`i'
replace nrsquare=e(r2) if run==`i'

drop beta_`i'1 beta_`i'2  

local i=`i'+1
}

rename coeff_r1 nteta1
rename coeff_r3 nteta3

collapse (max) teta1 teta3 nteta1 nteta3 nrsquare, by(run)
format nteta1 - nrsquare %6.4f
save coeffC3_thetas, replace

*/

/****************************/
/** Case 4 :all theta      **/
/****************************/
use CLEANsimmatrix_ST

drop if age<45 & age>75		/* eliminates negative cumsr_ values */ 	

keep if (teta3 != teta1) & (comp1==1 & comp2==1 & comp3==1) & cumsr>0
gen icumsr=1/cumsr 

gen change= (ctry!=ctry[_n-1])
gen run=sum(change)
drop change ctry

keep run age cumsr icumsr teta1 teta3 
order run age cumsr icumsr teta1 teta3 

/*it merges each simulated population (under a combination of teta1 and teta3) with
the estimated coefficients of beta in the equation cumsr(x)= alpha + beta1* teta1 + beta3* teta3 */
  
sort age
merge age using coeffC4_betas
sort run age

keep age run cumsr icumsr teta1 teta3 alpha beta1 beta3 rsquare
order age run cumsr icumsr teta1 teta3 alpha beta1 beta3 rsquare

/*this part creates a file with regression coefficents from the expression 

    cumsr(x) - alpha= r1 beta1 + r3 beta3

where r1hat is the estimate of teta1_ and r3hat is the estimate of teta3_ */
 
gen nicumsr= icumsr - alpha     
gen coeff_r1=.
gen coeff_r3=.
gen nrsquare=.

local i=1 
while `i'<=210  {   

reg nicumsr beta1 beta3 if run==`i', noc 
matrix beta_`i'=e(b)
svmat beta_`i'
local nrsquare_`i'=e(r2)

replace coeff_r1=beta_`i'[1,1] if run==`i'
replace coeff_r3=beta_`i'[1,2] if run==`i'
replace nrsquare=e(r2) if run==`i'

drop beta_`i'1 beta_`i'2  

local i=`i'+1
}

rename coeff_r1 nteta1
rename coeff_r3 nteta3

collapse (max) teta1 teta3 nteta1 nteta3 nrsquare, by(run)
format nteta1 - nrsquare %6.4f
save coeffC4_thetas, replace


end

