program drop _all
drop _all
program define CLEANsimulationmatrix
set more off

/*

	Produced in October  2011

0. CLEANsimulationmatrix.do invokes CLEANsimmatrix.do

1. CLEANsimulationmatrix.do is a matrix version of CLEANsimulation.do. 
Does not deal with underestimation of ages but 
with NET overestimation

2. It simulates outcomes when different values of teta1 
teta3, comp1 or comp3 are used. (there is a loop for this)
Two modalities: 

	MODALITY No 1 is with a loop
	MODALITY No 2 is with a single combination of values

3.IMPORTANT NOTES

3.1 First census is assumed to be January 0 and second census is
December 31, 1989. THUS WE DO NOT NEED TO USE DEATHS FROM 1990

3.2.We assume distortion ages at death follow same pattern of distortion as censuses

3.3. The parameters and functions required for this program are as follows:

3.3.1. Age misreporting :

mu1_x= standard proportion who overstate at age x ; 
mu1_x=prob of overstating age if individual is aged x (depend on x)

For values used see CR_agemistatement.do and related programs or output from Gilbert Brenes

We can increase (decrease) mu1_x by multiplying by a constant; remember that 
these are proportions and should not exceed 1.0. Multiplying values with constant
exceeding 2.8 lead to proportions over 1.

teta1= factor to modify prob age overstatement  
teta3= factor to modify prob of age at death overstatement

alphaj=the standard pattern of overstatement  (has values for j=1,2,...10)

alphaj is the prob that if somebody overstates age it does so by j years

k=the max Number of years of overstatement throughout is fixed at 10 years

The patterns of years of exaggeration are defined by the function alpha 
estimated from Costa Rica. Because the work in Costa Rica does not deal
with deaths, we assume the patterns are the same in deaths and population

   alphaj=prob de exagerar j a�os,una ecuacion que es funcion de j (pop)

we assume that this is a pattern of NET overstatement

For a good explanation of procedure to get alphaj see program 
CR_agemistatement.do. Here is a summary:

We use the probs estimated by Brenes with a multinomial (j=1,...10) model 
and as a function of age. We produce predicted values for each age of the probs of 
exagerar (disminuir) by j years. 

For age 10 the value represents the prob of exagerate or diminish by 10+. 
To correct for this we assume that prob of  exagerate or diminish goes down 
to 0 at age 15 and estimate prob of exagerate or diminish as PR(10+)*(2/5). 

For each j we now calculate the MEDIAN value  of the predicted probabilities 
produced across ages. We thus have 10 values of probs of exagerating (diminishing), 
one for each number of year of exageration. Call these MD(j).  We then estimate 
the relation ln(MD(j)/(1-MD(j))= a+b ln(j) and produce predicted values for each j. 
Finally, we add them all up, obtain the sum and standardize the predicted values by
this sum to make sure that the addition of the probs adds up to 1.0. We do the same
for both exageration and underestimation and for each age.

The values of alphaj used below are these final results and can be 
found in the file CR_FINAL_ERRORPATTERNS.DTA. 


Although below we only use the values of probs of OVERstateing 1,2,...10
we also estimated prob of UNDERstating by 1,2,...10. These are as follows:

local beta1=  .4820968        
local beta2=  .1999464        
local beta3=  .1054833         
local beta4=  .0649208         
local beta5=  .0440517         
local beta6=  .0319285         
local beta7=  .0242596         
local beta8=  .0190951         
local beta9=  .0154469         
local beta10= .0127711


3.3.2 Completeness

C1 and C2 = completeness of censuses at time 1 and time 2
C3= completeness of death registration

3.3.3. Parameters one needs to set at the outset:

teta1;C1 and C2..........OVERSTATEMENT OF AGES FOR MORTALITY AND CENSUSES (2) COMPLETENESS for censuses
teta3;C3.................OVERSTATEMENT FOR DEATHS and DEATH COMPLETENESS


Syntax is


	CLEANsimmatrix c1 c2 c3 teta1 teta3 run

NOTE: the parameter 'run' is needed to identify results of each combination
of c1 c2 c3 teta1 and teta3/ If one is running program only once, run==1


*/

/*IMPLEMENTATION OF MODALITY 1: A single combination of values
c1,c2,c3,teta1, teta3 plus the name of the run (last parameter) //pass only one set of parameters*/

*CLEANsimmatrix .9 .8 .85 2.5 2.5 1

*CLEANsimmatrix .9 .8 .85 0 0 1837
*CLEANsimmatrix .9 .8 .85 0.5 1.5 1846
*CLEANsimmatrix .9 .8 .85 1.5 1.5 1858
*CLEANsimmatrix .9 .8 .85 2.5 2.5 1872

*CLEANsimmatrix .8 .8 .85 0 0 37
*CLEANsimmatrix .8 .8 .85 0.5 1.5 46
*CLEANsimmatrix .8 .8 .85 1.5 1.5 58
*CLEANsimmatrix .8 .8 .85 2.5 2.5 72


/* MODALITY NO 2: We cycle over different combination of
parameters to run main program and produce populations
with different distortions*/

local run=1

  local c1=0.85
  while `c1'<=.85  { 

   local c2=0.95
   while `c2'<=0.95  { 

      local c3=0.85
      while `c3'<=0.85  {	 

          local teta1=0
          while `teta1'<=2.5  { 

	       		local teta3=0
		      	while `teta3'<=2.5  { 

CLEANsimmatrix `c1' `c2' `c3' `teta1' `teta3' `run' 

local run=`run' + 1

					local teta3=`teta3'+0.50
						              }

			local teta1=`teta1'+0.50
				              }

         local c3= `c3'+ 0.05
		             }

    local c2=`c2'+ 0.05
                   }

  local c1=`c1'+ 0.05
   
  }


use CLEANsimmatrixs_nst1_1, clear

local i=2
while `i' <=36  {
append using CLEANsimmatrixs_nst1_`i' 
local i=`i'+1
}

gen pop=1
gen model=4
gen scenario=1

saveold CLEANsimmatrixS1s_nst1, replace

*!erase CLEANsimmatrix_ST_*.dta
*END OF MODALITY No 2

end

/****************************************************************/
/****************************************************************/
/****************************************************************/
/****************************************************************/

program define CLEANsimmatrix
/*ST means stable population data produced with simulation_1.do */
use masterS_nst1, clear		
keep age px_1980 px_1990 dx_1980-dx_1989


/*

In this part of the program we set the values of all the parameters

k=10	max number of years of overstatement; will always be set to 10
C1	completeness first census
C2	completeness second census
C3	completeness of death registration
teta1   modifies pop age overstatement (when 1 we use standard)
teta3	modifies deaths age overstatement (when 1 we use standard)

*/

local k=10
local c1=`1'
local c2=`2'
local c3=`3'
local teta1=`4'
local teta3=`5'
local run=`6'

			/*end of definition of parameters*/


/****************to implement overstatement in POPULATIONS*****************
We define the LEVELS of population age overstatement with parameters TETA1
***************************************************************************/

gen muxpop=0
gen muxmort=0
replace muxpop=exp(-2.127674+.0136934*age)/(1+ exp(-2.127674+.0136934*age)) if age>=45
replace muxmort=muxpop if age>=45

/*
we alter standard values of mux to create different levels of
over statement of population age and of death ages
*/

local teta1=`4'
local teta3=`5'

replace muxpop=`teta1'*muxpop if age>=45    /*proportion overstating ages above 45 only*/
replace muxpop=0 if age==100
replace muxmort=`teta3'*muxmort if age>=45  /*proportion overstating ages above 45 only*/
replace muxmort=0 if age==100

local alpha1= .6205334   
local alpha2= .1964748           
local alpha3= .0793082           
local alpha4= .0397628           
local alpha5= .0229676           
local alpha6= .0145954           
local alpha7= .0099264           
local alpha8= .0071005           
local alpha9=  .0052806           
local alpha10= .0040503           


			/*BEGINS DISTORTIONS*/

/*
generates a data set having 101 columns in the following form

	1-O1	0	0	0....
	O1	1-O2	0	0....
	O1	O2	1-O3	0...


for both population and mortality

*/

local g=1
while `g'<=101 {
gen muxpop`g'=1
local g=`g'+1
}

local g=1
while `g'<=101 {
replace muxpop`g'=muxpop[`g']
replace muxpop`g'=1-muxpop if _n==`g'
replace muxpop`g'=0 if _n<`g'

local g=`g'+1
}

save muxpop.dta, replace

local g=1
while `g'<=101 {
gen muxmort`g'=1
local g=`g'+1
}

local g=1
while `g'<=101 {
replace muxmort`g'=muxmort[`g']
replace muxmort`g'=1-muxmort if _n==`g'
replace muxmort`g'=0 if _n<`g'

local g=`g'+1
}

save muxmort.dta, replace

/*
generates a data set having 101 columns in the following form

	1	0	0	0....
	alpha1	1	0	0....
	alpha2	alpha1	1	0...
	alpha3	alpha2  alpha1	1....
	alpha4	alpha3	alpha2	alpha1	1

*/

local j=1
while `j'<=101 {
gen ALPHA`j'=0
local j=`j'+1
}

local j=1
while `j'<=101{
replace ALPHA`j'= 0 if _n<`j'
replace ALPHA`j'=1 if `j'==_n

	local r=1
	while `r'<=10{
	replace ALPHA`j'= `alpha`r'' if _n-`j'==`r' & `r'<=10
	local r=`r'+1
	}

local j=`j'+1
}

save alpha.dta, replace

/*we have the MU's and the ALPHA's: we now multiply them together
to get main transtion matrix that distorts bth populations and deaths*/

local f=1
while `f'<=101{
gen mainpop`f'=muxpop`f'*ALPHA`f'
local f=`f'+1
}

local f=1
while `f'<=101{
gen mainmort`f'=muxmort`f'*ALPHA`f'
local f=`f'+1
}


order age mainpop* mainmort*

save transition.dta, replace

mkmat mainpop1-mainpop101, matrix(TRANSITIONPOP)
mkmat mainmort1-mainmort101, matrix(TRANSITIONMORT)

/*distortions for population 1*/

replace px_1980=px_1980*(0.75/0.85) if age>=15 & age<36

mkmat px_1980, matrix(POPTRUE1)
matrix POPERROR1=`c1'*TRANSITIONPOP*POPTRUE1
matrix POP1=POPTRUE1, POPERROR1

preserve
keep age   

mat colnames POP1 = px_1980 No1x
svmat double POP1, names(col)
sort age
save POP1.dta, replace

restore


/*distortions for population 2*/

replace px_1990=px_1990*(0.85/0.95) if age>=15 & age<36

mkmat px_1990, matrix(POPTRUE2)
matrix POPERROR2=`c2'*TRANSITIONPOP*POPTRUE2
matrix POP2=POPTRUE2, POPERROR2

preserve

keep age  

mat colnames POP2 = px_1990 No2x
svmat double POP2, names(col)
sort age
save POP2.dta, replace

restore

/*works with deaths: adds intercensal deaths*/

local j=1980
while `j'<=1989 {
replace dx_`j'=dx_`j'*(0.80/0.85) if age>=15 & age<36 
local j=`j'+1
}

local j=1980
while `j'<=1989 {
mkmat dx_`j', matrix(DX`j')
matrix DDX`j'=`c3'*TRANSITIONMORT*DX`j'
local j=`j'+1
}

preserve
keep age


local g=1980
while `g'<=1989 {
matrix MORT`g'=DX`g', DDX`g'
mat colnames MORT`g'=dx_`g' ddx_`g'
svmat double MORT`g', names(col)
sort age
save MORT`g'.dta, replace
local g=`g'+1
}

/*we now put together all the data*/

use POP1
sort age
merge 1:1 age using POP2, generate(_merge1980)
sort age
local h=1981
while `h'<=1989{
merge 1:1 age using MORT`h', generate(_merge`h')
local h=`h'+1
}


drop _merge*
order age px_1980 No1x px_1990 No2x dx* ddx*
save distorted.dta, replace		/*this is the distorted data*/


clear matrix
*!del MORT*.dta
*!del POP*.dta
*!del mux*.dta
*!del alpha.dta
*!del transition.dta

local files1: dir "." files "MORT*.dta"
local files2: dir "." files "POP*.dta"
local files3: dir "." files "mux*.dta"
local files4: dir "." files "alpha*.dta"
local files5: dir "." files "transition.dta"

foreach file in `files1' `files2' `files3' `files4' `files5' {
 erase `file'
 }


					/*END OF DISTORTIONS*/


					/*BEGINS CALCULATIONS*/


				/*for calculations of cumSR and cumsr*/

				/*cumulates populations*/

gen double Pop1=sum(px_1980)
gen double Pop2=sum(px_1990)

scalar cc=Pop1[_N]
scalar ee=Pop2[_N]
gen double Cumpop1=cond(_n~=1,cc-Pop1[_n-1],cc)
gen double Cumpop2=cond(_n~=1,ee-Pop2[_n-1],ee)

gen double pop1=sum(No1x)
gen double pop2=sum(No2x)

scalar hh=pop1[_N]
scalar ii=pop2[_N]

gen double cumpop1=cond(_n~=1, hh-pop1[_n-1], hh)
gen double cumpop2=cond(_n~=1, ii-pop2[_n-1], ii)

				/*cumulates pop but only up to 85 (including 85)*/

gen double Truncpop1=Cumpop1-Cumpop1[87] if age<=85
gen double truncpop1=cumpop1-cumpop1[87] if age<=85

gen double Truncpop2=Cumpop2-Cumpop2[97] if age<=95
gen double truncpop2=cumpop2-cumpop2[97] if age<=95


				/*cumulates deaths*/

				/*calculates intercensal deaths to those aged x+ from age 0 on*/


local j=1980
while `j'<=1989{
gen double mytemp1`j'=0
gen double mytemp2`j'=0
	local v=1+(`j'-1980)
	while `v'<=101{
	summarize dx_`j' if _n>=`v'
	replace mytemp1`j'=r(N)*r(mean) if _n==`v'
	summarize ddx_`j' if _n>=`v'
	replace mytemp2`j'=r(N)*r(mean) if _n==`v'
	local v=`v'+1
	}
local j=`j'+1
}


gen double cohort_Dx=0		/*true deaths to those aged x in first census*/
gen double cohort_dox=0		/*distorted deaths to those aged x in first census*/

local j=1980
local h=0
while `j'<=1989{
replace cohort_Dx=cohort_Dx + mytemp1`j'[_n+`h'] 
replace cohort_dox=cohort_dox + mytemp2`j'[_n+`h'] 
local h=`h'+1
local j=`j'+1
}

				/*cumulates intercensal deaths but only up to 85(including 85)*/

local j=1980
while `j'<=1989{
gen double mytemp3`j'=0
gen double mytemp4`j'=0
	local v=1+(`j'-1980)
	while `v'<=96{
	summarize dx_`j' if _n>=`v'&_n<=86+(`j'-1980)
	replace mytemp3`j'=r(N)*r(mean) if _n==`v'
	summarize ddx_`j' if _n>=`v'&_n<=86+(`j'-1980)
	replace mytemp4`j'=r(N)*r(mean) if _n==`v'
	local v=`v'+1
	}
local j=`j'+1
}

gen double trunc_Dx=0
gen double trunc_dox=0

local j=1980
local h=0
while `j'<=1989{
replace trunc_Dx=trunc_Dx + mytemp3`j'[_n+`h'] 
replace trunc_dox=trunc_dox + mytemp4`j'[_n+`h'] 
local h=`h'+1
local j=`j'+1
}



drop mytemp*


				/*cumSR and cumsr */

gen double cumSR=.
gen double cumsr=.
gen double truncSR=.
gen double truncsr=.

replace cumSR=1/((Cumpop1-cohort_Dx)/Cumpop2[_n+10])  if age<=90			/*cumul true actual to expected ratio*/
replace cumsr=1/((cumpop1-cohort_dox)/cumpop2[_n+10]) if age<=90			/*cumul observed actual to expected ratio*/

replace truncSR=1/((Truncpop1-trunc_Dx)/Truncpop2[_n+10])  if age<=90			/*trunc cumul true actual to expected ratio*/
replace truncsr=1/((truncpop1-trunc_dox)/truncpop2[_n+10]) if age<=90			/*trunc cumul observed actual to expected ratio*/




				/*calculates single year age intercensal deaths*/


gen double inter_Dx=dx_1980		/*intercensal true deaths*/
gen double inter_dox=ddx_1980		/*intercensal distorted deaths*/

local j=1981
while `j'<=1989 {
replace inter_Dx=inter_Dx + dx_`j'
replace inter_dox=inter_dox + ddx_`j'
local j=`j'+1
}
                                
				/*true and observed intercensal death rates*/


gen double mx=inter_dox/(10*(.5*(No1x+No2x)))
gen double Mx=inter_Dx/(10*(.5*(px_1980+px_1990)))



/*SUMMARY OF WHAT WE HAVE:

k...................proportion understating in deaths
teta1...............parameter level of overstatement in pop
teta3...............parameter level of overerstatement in deaths
px_1980.............true population in 1980
px_1990.............true population in 1990
No1x................observed population in 1980
No2x................observed populationin 1990
dx_`j'..............true deaths by age for 1980-1989
ddx_`j'.............observed deaths by age for 1980-1985
cohort_Dox..........true intercensal deaths
cohort_dox..........distorted intercensal deaths
inter_Dox...........total true deaths in age x over intercensal period
period_dox..........total observed deaths in age x over intercensal period
ratiopop_1..........observed to true pop in 1980
ratiopop_2..........observed to true pop in 1990
ratiocoh_deaths.....observed to true cohort deaths
ratiointer_deaths...observed to true inter censal deaths

cumSR..............cumul true ratios of actual to expected population
cumsr..............cumul observed ratios of actual to expected populations 

mx.................observed intercensal death rates
Mx.................true intercensal death rates

*/


gen comp1=float(`c1')		/*note that these simply define all these as float but that stata stores them as double*/
gen comp2=float(`c2')
gen comp3=float(`c3')
gen teta1=float(`teta1')
gen teta3=float(`teta3')
gen k=float(`k')
gen run=float(`run')

replace comp1=0.75 if age>=15 & age<36
replace comp2=0.85 if age>=15 & age<36
replace comp3=0.80 if age>=15 & age<36

				/*file to analyse effects of distortions*/


order run age teta1 teta3 k comp1 comp2 comp3 No1x cumpop1 px_1980 Cumpop1 No2x cumpop2 px_1990 Cumpop2 ddx* inter_dox cohort_dox dx* inter_Dx cohort_Dx cumsr cumSR truncSR truncsr mx Mx 
keep  run age teta1 teta3 k comp1 comp2 comp3 No1x cumpop1 px_1980 Cumpop1 No2x cumpop2 px_1990 Cumpop2 ddx* inter_dox cohort_dox dx* inter_Dx cohort_Dx cumsr cumSR truncSR truncsr mx Mx 

			/*variables to produce file exactly as needed to estimate completeness*/

rename run ctry

gen pm1980=No1x
gen pf1980=No1x
gen pm1990=No2x
gen pf1990=No2x

local r=1980
while `r'<=1989 {
gen dm`r'= ddx_`r'
gen df`r'= ddx_`r'
local r=`r'+1
            }

			/*group population into 5-year groups */

*ren age ageold
*gen age=5*int(ageold/5)
recode age 85/100=85
collapse (mean) ctry (median) teta1-comp3 (sum) pm1980-df1989, by(age)

keep ctry age teta1 teta3 comp1 comp2 comp3 pm1980 pf1980 pm1990 pf1990 dm1980-dm1989 df1980-df1989 
order ctry age teta1 teta3 comp1 comp2 comp3 pm1980 pf1980 pm1990 pf1990 dm1980-dm1989 df1980-df1989 

saveold CLEANsimmatrixs_nst1_`6'.dta, replace

end
/*********************/

quietly CLEANsimulationmatrix
