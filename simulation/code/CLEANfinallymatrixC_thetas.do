clear
program drop _all
drop _all

/****************************************************************************************/
program define CLEANfinallymatrixC_thetas
set more off


/*

MAY 2014

IMPORTANT: CURRENT VERSION IS BUILT TO HANDLE RECOVERY DATA FROM COUNTRIES
THE PROGRAM APPLIES TWO METHODS TO HANDLE OPEN AGE GROUP 85+ THESE METHODS ARE
IN THE SUBROUTINES

		FINALLYMATRIX1
		FINALLYMATRIX2

MAY 2014!!!!!!!! FINALLYMATRIX2 was judged to be too sensitive and was abandoned

THIS PROGRAM IS THE SAME AS ORIGINAL PROGRAM CLEANfinallymatrix.do but set up to
recover a distorted population that has an open age group at 85+ . This is 
done using ONLY ONE OF TWO methods. 

Method USE is FINALLYMATRIX1 

Method consist of using matrices for ages 5 to 85+ (81 age groups)
Calculates an auxiliar function ADJCUMSR4 which should be correct
when there is 85+ open age group

Run by invoking finallymatrix1


THE SECOND METHOD (FINALLYMATRIX2.DO) IS INCLUDED IN CLEANfinallymatrixC.do

******************************************************************************************
*****************************************************************************************/
/*
parameters 2020 0.960 0.974 0 0 1 arg_85s 1947 5 1960 9 1.040 1    
parameters 2020 0.981 1.005 0 0 1 arg_85s 1960 9 1970 9 1.040 2 
parameters 2020 0.984 0.990 0 0 1 arg_85s 1970 9 1980 10 1.040 3 
parameters 2020 0.997 0.988 0 0 1 arg_85s 1980 10 1991 5 1.040 4  
parameters 2020 1.033 0.985 0 0 1 arg_85s 1991 5 2001 11 1.040 5 
parameters 2020 1.033 0.995 0 0 1 arg_85s 2001 11 2010 10 1.040 6 
*
parameters 2020 0.940 0.969 0 0 2 arg_85s 1947 5 1960 9 1.040 7
parameters 2020 0.995 0.980 0 0 2 arg_85s 1960 9 1970 9 1.040 8
parameters 2020 0.987 0.987 0 0 2 arg_85s 1970 9 1980 10 1.040 9
parameters 2020 0.987 0.986 0 0 2 arg_85s 1980 10 1991 5 1.040 10
parameters 2020 1.022 0.987 0 0 2 arg_85s 1991 5 2001 11 1.040 11 
parameters 2020 1.022 0.990 0 0 2 arg_85s 2001 11 2010 10 1.040 12 


parameters 2070 1.001 0.885 0 0 1 bra_85s 1980 9 1991 9 0.868 13
parameters 2070 1.021 0.970 0 0 1 bra_85s 1991 9 2000 8 1.005 14
parameters 2070 1.003 0.996 0 0 1 bra_85s 2000 8 2010 8 1.005 15
*
parameters 2070 1.000 0.810 0 0 2 bra_85s 1980 9 1991 9 0.810 16
parameters 2070 1.035 0.950 0 0 2 bra_85s 1991 9 2000 8 0.955 17
parameters 2070 0.999 0.959 0 0 2 bra_85s 2000 8 2010 8 0.955 18


parameters 2120 1.020 0.852 0 0 1 chi_85s 1920 4 1930 4 1.040 19
parameters 2120 1.009 0.907 0 0 1 chi_85s 1930 4 1940 4 1.040 20
parameters 2120 1.064 0.934 0 0 1 chi_85s 1940 4 1952 4 1.040 21
parameters 2120 1.047 0.961 0 0 1 chi_85s 1952 4 1960 11 1.040 22
parameters 2120 1.035 0.955 0 0 1 chi_85s 1960 11 1970 4 1.040 23
parameters 2120 0.938 0.949 0 0 1 chi_85s 1970 4 1982 4 1.040 24 
parameters 2120 1.002 0.960 0 0 1 chi_85s 1982 4 1992 4 1.040 25
parameters 2120 0.996 0.977 0 0 1 chi_85s 1992 4 2002 4 1.040 26
parameters 2120 0.996 0.980 0 0 1 chi_85s 2002 4 2010 6 1.040 27
*
parameters 2120 1.016 0.882 0 0 2 chi_85s 1920 4 1930 4 1.040 28
parameters 2120 1.001 0.915 0 0 2 chi_85s 1930 4 1940 4 1.040 29
parameters 2120 1.056 0.932 0 0 2 chi_85s 1940 4 1952 4 1.040 30
parameters 2120 1.059 0.948 0 0 2 chi_85s 1952 4 1960 11 1.040 31
parameters 2120 1.074 0.957 0 0 2 chi_85s 1960 11 1970 4 1.040 32
parameters 2120 0.948 0.934 0 0 2 chi_85s 1970 4 1982 4 1.040 33
parameters 2120 1.008 0.955 0 0 2 chi_85s 1982 4 1992 4 1.040 34
parameters 2120 0.999 0.965 0 0 2 chi_85s 1992 4 2002 4 1.040 35
parameters 2120 0.999 0.978 0 0 2 chi_85s 2002 4 2010 6 1.040 36


parameters 2130 1.032 0.749 0 0 1 col_85s 1938 5 1951 5 1.040 37
parameters 2130 0.943 0.790 0 0 1 col_85s 1951 5 1964 7 1.040 38
parameters 2130 1.065 0.810 0 0 1 col_85s 1964 7 1973 10 1.040 39
parameters 2130 0.915 0.820 0 0 1 col_85s 1973 10 1985 10 1.040 40 
parameters 2130 1.061 0.830 0 0 1 col_85s 1985 10 1993 10 1.040 41 
parameters 2130 1.005 0.832 0 0 1 col_85s 1993 10 2005 5 1.040 42
parameters 2130 0.994 0.800 0 0 1 col_85s 2005 5 2012 6 1.040 43
*
parameters 2130 1.020 0.743 0 0 2 col_85s 1938 5 1951 5 1.040 44
parameters 2130 0.935 0.775 0 0 2 col_85s 1951 5 1964 7 1.040 45
parameters 2130 1.046 0.850 0 0 2 col_85s 1964 7 1973 10 1.040 46
parameters 2130 0.955 0.835 0 0 2 col_85s 1973 10 1985 10 1.040 47
parameters 2130 1.072 0.820 0 0 2 col_85s 1985 10 1993 10 1.040 48 
parameters 2130 1.006 0.825 0 0 2 col_85s 1993 10 2005 5 1.040 49
parameters 2130 0.992 0.835 0 0 2 col_85s 2005 5 2012 6 1.040 50


parameters 2140 0.906 0.893 0 0 1 cri_85s 1927 5 1950 5 1.180 51
parameters 2140 0.926 0.918 0 0 1 cri_85s 1950 5 1963 3 1.180 52
parameters 2140 0.979 0.935 0 0 1 cri_85s 1963 3 1973 5 1.168 53
parameters 2140 1.003 0.960 0 0 1 cri_85s 1973 5 1984 6 0.926 54
parameters 2140 1.041 0.970 0 0 1 cri_85s 1984 6 2000 6 1.201 55
parameters 2140 0.974 0.975 0 0 1 cri_85s 2000 6 2011 6 1.201 56
*
parameters 2140 0.901 0.882 0 0 2 cri_85s 1927 5 1950 5 1.180 57
parameters 2140 0.933 0.905 0 0 2 cri_85s 1950 5 1963 3 1.220 58
parameters 2140 0.969 0.928 0 0 2 cri_85s 1963 3 1973 5 1.129 59
parameters 2140 0.999 0.956 0 0 2 cri_85s 1973 5 1984 6 0.932 60
parameters 2140 1.078 0.957 0 0 2 cri_85s 1984 6 2000 6 1.139 61
parameters 2140 0.976 0.960 0 0 2 cri_85s 2000 6 2011 6 1.139 62


parameters 2150 1.065 0.800 0 0 1 cub_85s 1920 9 1931 9 1.016 63
parameters 2150 1.062 0.840 0 0 1 cub_85s 1931 9 1943 9 1.016 64
parameters 2150 1.061 0.870 0 0 1 cub_85s 1943 9 1953 1 1.016 65
parameters 2150 0.941 0.890 0 0 1 cub_85s 1953 1 1970 9 1.016 66
parameters 2150 1.031 0.983 0 0 1 cub_85s 1970 9 1981 9 0.891 67
parameters 2150 0.932 0.997 0 0 1 cub_85s 1981 9 2002 9 0.954 68
parameters 2150 0.979 0.989 0 0 1 cub_85s 2002 9 2010 6 0.954 69
* 
parameters 2150 1.063 0.830 0 0 2 cub_85s 1920 9 1931 9 1.016 70
parameters 2150 1.052 0.850 0 0 2 cub_85s 1931 9 1943 9 1.016 71
parameters 2150 1.050 0.865 0 0 2 cub_85s 1943 9 1953 1 1.016 72
parameters 2150 0.970 0.850 0 0 2 cub_85s 1953 1 1970 9 0.909 73
parameters 2150 1.002 0.961 0 0 2 cub_85s 1970 9 1981 9 0.930 74
parameters 2150 0.956 0.970 0 0 2 cub_85s 1981 9 2002 9 0.930 75
parameters 2150 0.970 0.980 0 0 2 cub_85s 2002 9 2010 6 0.930 76


parameters 2170 1.039 0.487 0 0 1 dre_85s 1935 5 1950 8 0.627 77
parameters 2170 1.002 0.500 0 0 1 dre_85s 1950 8 1960 8 0.627 78
parameters 2170 1.063 0.535 0 0 1 dre_85s 1960 8 1970 1 0.535 79
parameters 2170 0.930 0.563 0 0 1 dre_85s 1970 1 1981 12 0.924 80
parameters 2170 1.005 0.571 0 0 1 dre_85s 1981 12 1993 9 0.551 81
parameters 2170 1.038 0.594 0 0 1 dre_85s 1993 9 2002 10 0.594 82
parameters 2170 1.038 0.604 0 0 1 dre_85s 2002 10 2010 12 0.594 83
*************
* 
parameters 2170 1.022 0.485 0 0 2 dre_85s 1935 5 1950 8 0.627 84
parameters 2170 1.046 0.491 0 0 2 dre_85s 1950 8 1960 8 0.678 85
parameters 2170 1.054 0.530 0 0 2 dre_85s 1960 8 1970 1 0.580 86
parameters 2170 0.962 0.559 0 0 2 dre_85s 1970 1 1981 12 0.847 87
parameters 2170 0.976 0.579 0 0 2 dre_85s 1981 12 1993 9 0.679 88
parameters 2170 1.047 0.590 0 0 2 dre_85s 1993 9 2002 10 0.600 89
parameters 2170 1.020 0.600 0 0 2 dre_85s 2002 10 2010 12 0.600 90
*************

parameters 2180 0.993 0.738 0 0 1 ecu_85s 1950 11 1962 11 0.805 91
parameters 2180 0.951 0.811 0 0 1 ecu_85s 1962 11 1974 6 1.150 92
parameters 2180 1.094 0.817 0 0 1 ecu_85s 1974 6 1982 11 0.817 93
parameters 2180 1.096 0.859 0 0 1 ecu_85s 1982 11 1990 11 0.859 94
parameters 2180 1.027 0.800 0 0 1 ecu_85s 1990 11 2001 11 1.231 95
parameters 2180 1.015 0.805 0 0 1 ecu_85s 2001 11 2010 11 1.231 96
*
parameters 2180 0.998 0.754 0 0 2 ecu_85s 1950 11 1962 11 0.739 97
parameters 2180 0.988 0.776 0 0 2 ecu_85s 1962 11 1974 6 1.177 98
parameters 2180 1.085 0.803 0 0 2 ecu_85s 1974 6 1982 11 0.813 99
parameters 2180 1.093 0.832 0 0 2 ecu_85s 1982 11 1990 11 0.832 100
parameters 2180 1.022 0.795 0 0 2 ecu_85s 1990 11 2001 11 1.332 101
parameters 2180 1.025 0.801 0 0 2 ecu_85s 2001 11 2010 11 1.332 102


parameters 2190 1.059 0.721 0 0 1 els_85s 1930 5 1950 6 0.830 103
parameters 2190 1.042 0.700 0 0 1 els_85s 1950 6 1961 5 0.830 104
parameters 2190 0.978 0.744 0 0 1 els_85s 1961 5 1971 6 0.952 105
parameters 2190 0.964 0.702 0 0 1 els_85s 1971 6 1992 9 0.771 106
parameters 2190 0.958 0.714 0 0 1 els_85s 1992 9 2007 5 0.771 107

parameters 2190 0.958 0.714 0 0 1 els_85s 2007 5 2009 10 0.771 108
*************
*
parameters 2190 1.006 0.752 0 0 2 els_85s 1930 5 1950 6 0.830 109
parameters 2190 1.021 0.707 0 0 2 els_85s 1950 6 1961 5 0.852 110
parameters 2190 1.004 0.762 0 0 2 els_85s 1961 5 1971 6 0.950 111
parameters 2190 0.977 0.705 0 0 2 els_85s 1971 6 1992 9 0.757 112
parameters 2190 0.958 0.752 0 0 2 els_85s 1992 9 2007 5 0.757 113

parameters 2190 0.961 0.752 0 0 2 els_85s 2007 5 2009 10 0.757 114
*************
*/


parameters 2250 0.912 0.888 0 0 1 gua_85s 1950 4 1964 4 1.086 116
parameters 2250 1.147 1.289 0 0 1 gua_85s 1964 4 1973 3 0.824 117
parameters 2250 1.062 1.566 0 0 1 gua_85s 1973 3 1981 3 0.801 118
parameters 2250 1.026 1.079 0 0 1 gua_85s 1981 3 1994 4 1.005 119
parameters 2250 1.024 1.544 0 0 1 gua_85s 1994 3 2002 11 1.433 120
parameters 2250 1.003 1.544 0 0 1 gua_85s 2002 11 2009 6 1.433 121
*
parameters 2250 0.924 1.013 0 0 2 gua_85s 1950 4 1964 4 1.167 123
parameters 2250 1.105 1.285 0 0 2 gua_85s 1964 4 1973 3 0.825 124
parameters 2250 1.073 1.535 0 0 2 gua_85s 1973 3 1981 3 0.739 125
parameters 2250 0.984 1.040 0 0 2 gua_85s 1981 3 1994 4 1.025 126
parameters 2250 1.011 1.343 0 0 2 gua_85s 1994 3 2002 11 1.297 127
parameters 2250 1.009 1.343 0 0 2 gua_85s 2002 11 2009 6 1.297 128

*parameters 2250 0.883 0.784 0 0 1 gua_85s 1940 4 1950 4 1.086 115
*parameters 2250 0.912 0.888 0 0 1 gua_85s 1950 4 1964 4 1.086 116
*parameters 2250 1.147 0.905 0 0 1 gua_85s 1964 4 1973 3 0.824 117
*parameters 2250 1.062 0.893 0 0 1 gua_85s 1973 3 1981 3 0.801 118
*parameters 2250 1.026 0.899 0 0 1 gua_85s 1981 3 1994 4 1.005 119
*parameters 2250 1.024 0.900 0 0 1 gua_85s 1994 3 2002 11 1.433 120
*parameters 2250 1.003 0.940 0 0 1 gua_85s 2002 11 2009 6 1.433 121
*
*parameters 2250 0.896 0.709 0 0 2 gua_85s 1940 4 1950 4 1.086 122
*parameters 2250 0.924 0.850 0 0 2 gua_85s 1950 4 1964 4 1.167 123
*parameters 2250 1.105 0.890 0 0 2 gua_85s 1964 4 1973 3 0.825 124
*parameters 2250 1.073 0.873 0 0 2 gua_85s 1973 3 1981 3 0.739 125
*parameters 2250 0.984 0.888 0 0 2 gua_85s 1981 3 1994 4 1.025 126
*parameters 2250 1.011 0.875 0 0 2 gua_85s 1994 3 2002 11 1.297 127
*parameters 2250 1.009 0.900 0 0 2 gua_85s 2002 11 2009 6 1.297 128
/*
parameters 2280 0.923 0.495 0 0 1 hon_85s 1940 6 1945 6 0.409 129
parameters 2280 0.936 0.500 0 0 1 hon_85s 1945 6 1950 6 0.409 130
parameters 2280 0.926 0.518 0 0 1 hon_85s 1950 6 1961 4 0.409 131
parameters 2280 1.027 0.750 0 0 1 hon_85s 1961 4 1974 3 0.735 132
parameters 2280 0.920 0.750 0 0 1 hon_85s 1974 3 1988 5 1.117 133

parameters 2280 0.914 0.750 0 0 1 hon_85s 1988 5 1990 10 1.117 134
*************
*
parameters 2280 0.893 0.440 0 0 2 hon_85s 1940 6 1945 6 0.409 135
parameters 2280 0.905 0.455 0 0 2 hon_85s 1945 6 1950 6 0.409 136
parameters 2280 0.946 0.469 0 0 2 hon_85s 1950 6 1961 4 0.385 137
parameters 2280 0.995 0.730 0 0 2 hon_85s 1961 4 1974 3 0.711 138
parameters 2280 0.909 0.740 0 0 2 hon_85s 1974 3 1988 5 0.982 139

parameters 2280 0.911 0.740 0 0 2 hon_85s 1988 5 1990 10 0.982 140
*************

parameters 2310 1.044 0.752 0 0 1 mex_85s 1921 6 1930 6 1.040 141
parameters 2310 1.025 0.830 0 0 1 mex_85s 1930 6 1940 6 1.040 142
parameters 2310 1.003 0.883 0 0 1 mex_85s 1940 6 1950 6 1.040 143

parameters 2310 1.009 0.860 0 0 1 mex_85s 1950 6 1960 6 1.040 144
parameters 2310 1.031 0.822 0 0 1 mex_85s 1960 6 1970 1 1.040 145
parameters 2310 0.981 0.945 0 0 1 mex_85s 1970 1 1980 6 1.040 146
parameters 2310 1.037 0.940 0 0 1 mex_85s 1980 6 1990 3 1.040 147
parameters 2310 1.027 0.935 0 0 1 mex_85s 1990 3 2000 2 1.040 148
parameters 2310 1.021 0.959 0 0 1 mex_85s 2000 2 2010 6 1.040 149
*
parameters 2310 0.902 0.800 0 0 2 mex_85s 1921 6 1930 6 1.040 150
parameters 2310 0.934 0.883 0 0 2 mex_85s 1930 6 1940 6 1.040 151
parameters 2310 1.008 0.879 0 0 2 mex_85s 1940 6 1950 6 1.040 152

parameters 2310 1.200 0.843 0 0 2 mex_85s 1950 6 1960 6 1.040 153
parameters 2310 1.050 0.941 0 0 2 mex_85s 1960 6 1970 1 1.040 154
parameters 2310 1.016 0.950 0 0 2 mex_85s 1970 1 1980 6 1.040 155
parameters 2310 0.916 0.935 0 0 2 mex_85s 1980 6 1990 3 1.040 156
parameters 2310 0.972 0.943 0 0 2 mex_85s 1990 3 2000 2 1.040 157
parameters 2310 0.970 0.940 0 0 2 mex_85s 2000 2 2010 6 1.040 158

parameters 2340 0.918 0.498 0 0 1 nic_85s 1940 5 1950 5 0.542 159
parameters 2340 0.990 0.456 0 0 1 nic_85s 1950 5 1963 5 0.542 160
parameters 2340 0.941 0.630 0 0 1 nic_85s 1963 5 1971 4 0.603 161
parameters 2340 1.133 0.510 0 0 1 nic_85s 1971 4 1995 4 0.732 162
parameters 2340 0.935 0.551 0 0 1 nic_85s 1995 4 2005 5 0.732 163
parameters 2340 0.935 0.561 0 0 1 nic_85s 2005 5 2010 6 0.732 164
*
parameters 2340 0.908 0.471 0 0 2 nic_85s 1940 5 1950 5 0.542 165
parameters 2340 0.970 0.500 0 0 2 nic_85s 1950 5 1963 5 0.537 166
parameters 2340 0.955 0.610 0 0 2 nic_85s 1963 5 1971 4 0.588 167
parameters 2340 1.137 0.515 0 0 2 nic_85s 1971 4 1995 4 0.691 168
parameters 2340 0.940 0.510 0 0 2 nic_85s 1995 4 2005 5 0.691 169
parameters 2340 0.940 0.520 0 0 2 nic_85s 2005 5 2010 6 0.691 170


parameters 2350 1.050 0.829 0 0 1 pan_85s 1940 9 1950 12 1.040 171
parameters 2350 1.045 0.839 0 0 1 pan_85s 1950 12 1960 12 1.040 172 
parameters 2350 0.975 0.848 0 0 1 pan_85s 1960 12 1970 5 1.040 173
parameters 2350 1.061 0.855 0 0 1 pan_85s 1970 5 1980 5 1.040 174
parameters 2350 1.045 0.852 0 0 1 pan_85s 1980 5 1990 5 1.040 175
parameters 2350 0.997 0.850 0 0 1 pan_85s 1990 5 2000 5 1.040 176
parameters 2350 1.001 0.853 0 0 1 pan_85s 2000 5 2010 5 1.040 177
*
parameters 2350 1.070 0.705 0 0 2 pan_85s 1940 9 1950 12 1.040 178
parameters 2350 1.073 0.755 0 0 2 pan_85s 1950 12 1960 12 1.040 179 
parameters 2350 0.971 0.806 0 0 2 pan_85s 1960 12 1970 5 1.040 180
parameters 2350 1.039 0.800 0 0 2 pan_85s 1970 5 1980 5 1.040 181
parameters 2350 1.023 0.790 0 0 2 pan_85s 1980 5 1990 5 1.040 182
parameters 2350 0.990 0.770 0 0 2 pan_85s 1990 5 2000 5 1.040 183
parameters 2350 1.002 0.779 0 0 2 pan_85s 2000 5 2010 5 1.040 184


parameters 2360 0.967 0.601 0 0 1 par_85s 1950 10 1962 10 0.627 185
parameters 2360 0.990 0.619 0 0 1 par_85s 1962 10 1972 7 0.830 186
parameters 2360 0.994 0.606 0 0 1 par_85s 1972 7 1982 7 0.840 187
parameters 2360 1.017 0.550 0 0 1 par_85s 1982 7 1992 8 0.791 188
parameters 2360 0.988 0.612 0 0 1 par_85s 1992 8 2002 8 0.751 189
parameters 2360 0.988 0.681 0 0 1 par_85s 2002 8 2010 6 0.751 190
*
parameters 2360 0.980 0.543 0 0 2 par_85s 1950 10 1962 10 0.586 191
parameters 2360 1.025 0.550 0 0 2 par_85s 1962 10 1972 7 0.802 192
parameters 2360 0.968 0.561 0 0 2 par_85s 1972 7 1982 7 0.698 193
parameters 2360 1.039 0.530 0 0 2 par_85s 1982 7 1992 8 0.776 194
parameters 2360 0.981 0.560 0 0 2 par_85s 1992 8 2002 8 0.733 195
parameters 2360 0.970 0.631 0 0 2 par_85s 2002 8 2010 6 0.733 196


parameters 2370 1.102 0.490 0 0 1 per_85s 1940 6 1961 7 0.706 197
parameters 2370 1.025 0.500 0 0 1 per_85s 1961 7 1972 6 0.706 198
parameters 2370 1.035 0.495 0 0 1 per_85s 1972 6 1981 7 0.574 199
parameters 2370 1.006 0.522 0 0 1 per_85s 1981 7 1993 7 0.594 200
parameters 2370 0.990 0.533 0 0 1 per_85s 1993 7 2007 10 0.668 201

parameters 2370 0.990 0.533 0 0 1 per_85s 2007 10 2009 10 0.668 202
*************
*
parameters 2370 1.109 0.492 0 0 2 per_85s 1940 6 1961 7 0.706 203
parameters 2370 1.035 0.488 0 0 2 per_85s 1961 7 1972 6 0.628 204
parameters 2370 1.025 0.490 0 0 2 per_85s 1972 6 1981 7 0.552 205
parameters 2370 1.020 0.510 0 0 2 per_85s 1981 7 1993 7 0.584 206
parameters 2370 0.977 0.553 0 0 2 per_85s 1993 7 2007 10 0.626 207
parameters 2370 0.977 0.553 0 0 2 per_85s 2007 10 2009 10 0.626 208
*************

parameters 2460 1.041 0.960 0 0 1 uru_85s 1963 10 1975 5 1.015 209
parameters 2460 0.984 0.961 0 0 1 uru_85s 1975 5 1985 10 0.909 210
parameters 2460 0.985 0.974 0 0 1 uru_85s 1985 10 1996 5 1.009 211
parameters 2460 0.988 0.988 0 0 1 uru_85s 1996 5 2004 5 0.958 212
parameters 2460 0.996 0.996 0 0 1 uru_85s 2004 5 2011 12 0.958 213
*
parameters 2460 1.040 0.925 0 0 2 uru_85s 1963 10 1975 5 1.014 214
parameters 2460 0.985 0.939 0 0 2 uru_85s 1975 5 1985 10 0.902 215
parameters 2460 0.981 0.953 0 0 2 uru_85s 1985 10 1996 5 0.961 216
parameters 2460 0.952 0.961 0 0 2 uru_85s 1996 5 2004 5 0.900 217
parameters 2460 0.985 0.968 0 0 2 uru_85s 2004 5 2011 12 0.900 218


parameters 2470 1.090 0.846 0 0 1 ven_85s 1936 12 1941 12 0.902 219
parameters 2470 1.065 0.855 0 0 1 ven_85s 1941 12 1950 11 0.840 220
parameters 2470 1.091 0.866 0 0 1 ven_85s 1950 11 1961 2 0.902 221
parameters 2470 1.075 0.885 0 0 1 ven_85s 1961 2 1971 11 0.840 222
parameters 2470 1.030 0.866 0 0 1 ven_85s 1971 11 1981 10 1.052 223
parameters 2470 0.949 0.872 0 0 1 ven_85s 1981 10 1990 10 0.948 224
parameters 2470 1.030 0.893 0 0 1 ven_85s 1990 10 2001 10 0.968 225
parameters 2470 1.035 0.895 0 0 1 ven_85s 2001 10 2011 10 0.968 226

parameters 2470 1.051 0.851 0 0 2 ven_85s 1936 12 1941 12 1.038 227
parameters 2470 1.036 0.850 0 0 2 ven_85s 1941 12 1950 11 0.954 228
parameters 2470 1.061 0.859 0 0 2 ven_85s 1950 11 1961 2 1.038 229
parameters 2470 1.076 0.861 0 0 2 ven_85s 1961 2 1971 11 0.954 230
parameters 2470 1.012 0.898 0 0 2 ven_85s 1971 11 1981 10 1.107 231
parameters 2470 0.955 0.874 0 0 2 ven_85s 1981 10 1990 10 1.019 232
parameters 2470 1.044 0.845 0 0 2 ven_85s 1990 10 2001 10 0.970 233
parameters 2470 1.005 0.842 0 0 2 ven_85s 2001 10 2011 10 0.970 234
*/
end


/***********************************/
program define parameters
set more off

local counter=1
local teta3=`5'
 while `teta3'<=2.5 {
  local teta1=`4'    
    while `teta1'<=2.5 {

finallymatrix1 `1' `2' `3' `teta1' `teta3' `6' `7' `8' `9' `10' `11' `12' `counter' 
local counter=`counter'+1

    local teta1=`teta1'+.5
                        }
 local teta3=`teta3'+.5
                      }


use CLEANcumsr_`1'_`8'_`6'_1, replace


local run=2
while `run'<=(`counter'-1){
append using CLEANcumsr_`1'_`8'_`6'_`run'
local run=`run'+1
}
keep if age>=45

save allCLEANcumsr_`1'_`8'_`6', replace
drop _all


*!erase CLEANcumsr*


/** MAKE CALCULATIONS TO FIND BEST THETA1 & THETA3 **/

use allCLEANcumsr_`1'_`8'_`6', clear
sort run age

gen delta1=abs(adjcumsr1-1)
bysort run: egen dsum=sum(delta1) if age>=45 & age<=65

l ctry run myear age adjcumsr1 delta1 teta1 teta3 dsum if age==65
bysort dsum: l ctry run myear age adjcumsr1 delta1 teta1 teta3 dsum if age==65

keep if _n==1

saveold CLEANcumsr_`13', replace 

/*
use CLEANcumsr_1, clear
local i=2
 while `i' <=192 /*234*/  {
append using CLEANcumsr_`i' 

local i=`i'+1
}

keep ctry myear sex teta1 teta3 adjcumsr1 delta1 dsum run
order ctry myear sex teta1 teta3 adjcumsr1 delta1 dsum run

saveold CLEANcumsr_thetas_ctry, replace
*!erase CLEANcumsr_*.dta 
*/    
end


/****************************************************************************************/
/****************************************************************************************/
program define finallymatrix1

/* This is main program

LAST WRITTEN ON OCTOBER 2011 PROGRAM IS SET TO RUN TO PRODUCE FINAL ADJUSTED VALUES OF 
MORTALITY RATES AS WELL AS ADJUSTED LIFE TABLES WHERE THE ADJUSTMENT INCLUDE COMPLETENESS
AND AGE MISSTATEMENT


1) this program reads two censuses and intercensal deaths, calculates
and adjusted populations and deaths after adjusting 

a. Needs parameters of age overstatement, teta1 and teta3
b. Needs parameters of completeness of census and death registration

2) Takes the values of teta1, teta3, and 
the estimates of completeness to generate adjusted values of Mx and
then calls a life table program to generate the life table.

3) Essentially what the program does is to UNDO what the program CLEANsimulationmatrix.do
does to introduce distortions

4) The last part of the rogram calculates life tables by single years of age using
adjusted and unadjusted Mx's

The syntax is 

		finallymatrix1 c h j t1 t2 g x y1 m1 y2 m2 jj

	1	c is country code
	2	h is C2/C1 from brass
	3	j is C3/(.5*(C1+C2)) from BH adjusted for rate of growth
	4  	t1 is teta1
	5	t3 is teta3
	6	g is 1 if male and 2 if females
	7	x is name of file (structure identical to the files used by REAL-ESTIMATION.DO
	8	y1 is the first year of census
	9	m1 is the month of first census
	10	y2 is the year of second census
	11	m2 is the month of the second census
	12	similar to 2 but not adjusted by rate of growth
	13	counter
*/

use `7'
keep if ctry==`1' 
gen sex=cond(`6'==1,1,2)
										/*RENAMES locals */

local ctry = `1'
local yr1=`8'
local yr2=`10'
local teta1=`4'
local teta3=`5'
local adjust1=`2'
local adjust2=`3'
local adjust3=`12'
local number=`13' 
local cm2=12*(`yr2'-1900)+(`11'-1) 				/*century month*/
local cm1=12*(`yr1'-1900)+(`9'-1) 				/*century month*/
scalar exposure=(`cm2'-`cm1')/12				/*in years*/
gen year=`yr1'
local dif= `yr2'-`yr1'							/*length of intercensal period*/

local k=trunc((`yr1'+`yr2')/2) 

if `6'==1{

keep ctry sex age pm`yr1' pm`yr2' dm`yr1'- dm`yr2' 
gen No1x=pm`yr1'
gen No2x=pm`yr2'
rename pm`yr1' px_`yr1'
rename pm`yr2' px_`yr2'

	local h=`yr1'
	while `h'<=`yr2' {
	gen ddx_`h'=dm`h'
	rename dm`h' dx_`h' 
	local h=`h'+1
	}

}
else {

keep ctry sex age pf`yr1' pf`yr2' df`yr1'-df`yr2' 
gen No1x=pf`yr1'
gen No2x=pf`yr2'
rename pf`yr1' px_`yr1'
rename pf`yr2' px_`yr2'

	local h=`yr1'
	while `h'<=`yr2' {
	gen ddx_`h'=df`h'
	rename df`h' dx_`h' 
	local h=`h'+1
	}
}

drop if age==0

save mytemp.dta, replace
								/*Basic functions*/

/*

The task is to take the estimated teta1, test3, and C2/C1 as well as C3/(.5*(C1+C2)
and calculate adjusted expected/observed population ratios as well as death rates. These
wil be corrected for both completeness and age overstatement

Fot this what matters is the following:

A. Age misreporting :

mu_x= standard proportion who overstate at age x; 
nu_x= standard proportion who overstate ages at death;

Because the work we know of only deals with over statement 
of populations we will assume that mu_x=nu_x

We can increase (decrease) mu_x and nu_x by multiplying by a constant;
remember though that these are proportions and should not exceed 1.0

teta1= factor to modify mu_x  
teta3= factor to modify nu_x

alphaj=the standard pattern of overstatement  (has values for j=1,2,...10)
alphaj is the prob that if somebody overstates age it does so by j years

k=the max Number of years of overstatement throughout is fixed at 10 years

C1 and C2 = completeness of censuses at time 1 and time 2
C3= completeness of death registration

B. Patterns of age ovestatement

The patterns of ovestatement by years of exaggeration or reduction 
are defined by alpha(j) 

   alpha(j)=prob de exagerar j años,una ecuacion que es funcion de j (pop)
   
For a good explanation of procedure to get alphaj and betaj see program 
CR_agemistatement.do. Here is a summary:

We use the probs estimated by Brenes with a multinomial (for j=1,...10) model 
and as function of age. We produce predicted values for each age of the probs of 
exagerar (disminuir) by j years. 

For age 10 the value reprpesents the prob of exagerate or diminish by 10+. 
To correct for this we assume that prob of  exagerate or diminish goes down 
to 0 at age 15 and estimate prob of exagerate or diminish as PR(10+)*(2/5). 

For each j we now calculate the MEDIAN value  of the predicted probabilities 
produced across ages. We thus have 10 values of probs of exagerating (diminishing), 
one for each number of year of exageration. Call these MD(j).  WE then estimate the 
relation ln(MD(j)/(1-MD(j))= a+b ln(j) and produce predicted values for each j. Finally, 
we add them all up, obtain the sum and standardize the predicted values by this sum 
to make sure that the addition of the probs adds up to 1.0. We do the same for both 
exageration and underestimation.

The values of alphaj and beta j used below are these final results and can be found in 
the file CR_FINAL_ERRORPATTERNS.DTA

local alpha1= .6205334   
local alpha2= .1964748           
local alpha3= .0793082           
local alpha4= .0397628           
local alpha5= .0229676           
local alpha6= .0145954           
local alpha7= .0099264           
local alpha8= .0071005           
local alpha9=  .0052806           
local alpha10= .0040503           

*/


/*Defines quantities for the transition matrix that produces distortions of ages*/


gen muxpop=0
gen muxmort=0
replace muxpop=exp(-2.127674+.0136934*age)/(1+ exp(-2.127674+.0136934*age)) if age>=45

/*important change to deal with open age group: prob of overstating if in 85+ is ZERO*/

replace muxpop=0 if age>=85

replace muxmort=muxpop if age>=45


/*
we alter standard values of mux to create different levels of
over statement of population age and of death ages
*/

replace muxpop=`teta1'*muxpop if age>=45    /*proportion overstating ages above 45 only*/
replace muxpop=0 if age==85
replace muxmort=`teta3'*muxmort if age>=45  /*proportion overstating ages above 45 only*/
replace muxmort=0 if age==85

local alpha1= .6205334   
local alpha2= .1964748           
local alpha3= .0793082           
local alpha4= .0397628           
local alpha5= .0229676           
local alpha6= .0145954           
local alpha7= .0099264           
local alpha8= .0071005           
local alpha9=  .0052806           
local alpha10= .0040503          

 

/************************BEGINS RECOVERY******************/


/************************IMPORTANT*************************

In cases of simulated populations we use 0-85 and matrix must be of n=86
In cases of observed countries we use 5-85 and matrix must be of n=81

*********************************************************/

/*

generates a data set having 81 columns in the following form

	1-O1	0	0	0....
	O1	1-O2	0	0....
	O1	O2	1-O3	0...


for both population and mortality

*/


local g=1
while `g'<=81 {						/****:for real pops should be 81 (5-85)***/
gen muxpop`g'=1
local g=`g'+1
}

local g=1
while `g'<=81 {
replace muxpop`g'=muxpop[`g']
replace muxpop`g'=1-muxpop if _n==`g'
replace muxpop`g'=0 if _n<`g'

local g=`g'+1
}

save muxpop.dta, replace

local g=1
while `g'<=81 {
gen muxmort`g'=1
local g=`g'+1
}

local g=1
while `g'<=81 {
replace muxmort`g'=muxmort[`g']
replace muxmort`g'=1-muxmort if _n==`g'
replace muxmort`g'=0 if _n<`g'
local g=`g'+1
}

save muxmort.dta, replace

/*
generates a data set having 81 columns in the following form

	1	0	0	0....
	alpha1	1	0	0....
	alpha2	alpha1	1	0...
	alpha3	alpha2  alpha1	1....
	alpha4	alpha3	alpha2	alpha1	1

*/

local j=1
while `j'<=81 {
gen ALPHA`j'=0
local j=`j'+1
}

local j=1
while `j'<=81{
replace ALPHA`j'= 0 if _n<`j'
replace ALPHA`j'=1 if `j'==_n

	local r=1
	while `r'<=10{
	replace ALPHA`j'= `alpha`r'' if _n-`j'==`r' & `r'<=10
	local r=`r'+1
	}

local j=`j'+1
}

save alpha.dta, replace

/**************************************************************************
we now change the alpha's for ages 76-84 so that the last values is the sum
of alphas's that will not be used when the last age group is 85+:

for age 76 the last alpha must be alpha9+alpha10
for age 77 the last alpha must be alpha8+alpha9+alpha10
......
for age 84 the last alpha must be alpha2+......+alpha10

**************************************************************************/

local newalpha1=`alpha10'+`alpha9'
local newalpha2=`newalpha1'+`alpha8'
local newalpha3=`newalpha2'+`alpha7'
local newalpha4=`newalpha3'+`alpha6'
local newalpha5=`newalpha4'+`alpha5'
local newalpha6=`newalpha5'+`alpha4'
local newalpha7=`newalpha6'+`alpha3'
local newalpha8=`newalpha7'+`alpha2'
local newalpha9=`newalpha8'+`alpha1'

local z=72
while `z'<=80 {
local v=`z'-71
replace ALPHA`z'=`newalpha`v'' if _n==81
local z=`z'+1
}


save alpha.dta, replace


/*
we have the MU's and the ALPHA's: we now multiply them together
to get main transtion matrix that distorts bth populations and deaths
*/

local f=1
while `f'<=81{
gen mainpop`f'=muxpop`f'*ALPHA`f'
local f=`f'+1
}

local f=1
while `f'<=81{
gen mainmort`f'=muxmort`f'*ALPHA`f'
local f=`f'+1
}

order age mainpop* mainmort*

save transition.dta, replace

mkmat mainpop1-mainpop81, matrix(TRANSITIONPOP)           
mkmat mainmort1-mainmort81, matrix(TRANSITIONMORT)

/*use inverse of transition matrices to recover populations and deaths*/


/*for census No 1*/

mkmat No1x, matrix(POPOBS1)
matrix POPEST1=inv(TRANSITIONPOP)*POPOBS1
matrix POP1=POPOBS1, POPEST1

preserve
keep age   

mat colnames POP1 = No1x popest1x
svmat double POP1, names(col)
sort age
save POP1est.dta, replace

restore

/*for census No 2*/

mkmat No2x, matrix(POPOBS2)
matrix POPEST2=inv(TRANSITIONPOP)*POPOBS2
matrix POP2=POPOBS2, POPEST2

preserve

keep age  

mat colnames POP2 = No2x popest2x
svmat double POP2, names(col)
sort age
save POP2est.dta, replace

restore

/*Deaths: adds intercensal deaths OJO: in observed cases loop must include deaths in year of second census*/

local j=`yr1'
while `j'<=`yr2'{
mkmat ddx_`j', matrix(DDX`j')
matrix DESTX`j'=inv(TRANSITIONMORT)*DDX`j'
local j=`j'+1
}

preserve
keep age

local g=`yr1'
while `g'<=`yr2' {
matrix MORT`g'=DDX`g', DESTX`g'
mat colnames MORT`g'=ddx_`g' destx_`g'
svmat double MORT`g', names(col)
sort age
save MORTest`g'.dta, replace
local g=`g'+1
}

								/*we now put together all the data*/
use POP1est
sort age
merge 1:1 age using POP2est, generate(_merge`yr1')
sort age

local h=`yr1'+1
while `h'<=`yr2'{
merge 1:1 age using MORTest`h', generate(_merge`h')
local h=`h'+1
}

save recovered.dta, replace		/*this is the initial recovered data*/

clear matrix

!erase MORT*.dta
!erase POP*.dta
!erase mux*.dta
!erase transition.dta
!erase alpha.dta
									/*END OF RECOVERY*/
									/*Computation of intercensal deaths*/

gen obsintercensal=0
gen adjintercensal=0

replace obsintercensal=((12-(`9'-1))/12)* ddx_`yr1'+ ((`11'-1)/12)*ddx_`yr2' 
replace adjintercensal=((12-(`9'-1))/12)* destx_`yr1'+((`11'-1)/12)*destx_`yr2'

local t=`yr1'+1
while `t'<=`yr2'-1 {	 
replace obsintercensal=obsintercensal+ddx_`t'
replace adjintercensal=adjintercensal+destx_`t'
local t=`t'+1
}

		/*calculates adjusted death rates for intercensal period*/


gen double adjMx1=(adjintercensal/(exposure*.5*(popest1x+popest2x)))*(1/`adjust2')  /*adjusted for everything with BH*/
gen double adjMx2=(obsintercensal/(exposure*.5*(No1x+No2x)))*(1/`adjust2') 			/*adjusted only for completeness with BH*/
gen double obsMx=(obsintercensal/(exposure*.5*(No1x+No2x)))							/*not adjusted at all=observed*/


/*CALCULATE RATIOS OBSERVED TO EXPECTED BEFORE and AFTER ADJUSTING FOR AGE MISSTATEMENT AND COMPLETENESS*/


		/*Calculates deaths at ages x+ during intercensal period*/


gen double adjcohort_dx=0
gen double obscohort_dx=0
gen double newadjcohort_dx=0
gen double newobscohort_dx=0




replace destx_`yr1'=destx_`yr1'*((12-(`9'-1))/12)	/*census 1: EXCLUDE fraction of deaths in first part of first year*/ 
replace destx_`yr2'=destx_`yr2'*((`11'-1)/12)		/*census 2: INCLUDE ONLY fraction of deaths in first part of second year*/

replace ddx_`yr1'=ddx_`yr1'*((12-(`9'-1))/12) 			 
replace ddx_`yr2'=ddx_`yr2'*((`11'-1)/12)

/***********************IGNORING THAT THERE IS AN open age group at 85+*******************************/

local j=`yr1'
while `j'<=`yr2'{
gen double mytemp1`j'=0
gen double mytemp2`j'=0
	local v=1+(`j'-`yr1')
	while `v'<=81 {
	summarize destx_`j' if _n>=`v'
	replace mytemp1`j'=r(N)*r(mean) if _n==`v'
	summarize ddx_`j' if _n>=`v'
	replace mytemp2`j'=r(N)*r(mean) if _n==`v'
	local v=`v'+1
	}
local j=`j'+1
}


/*************NOT IGNORING age group at 85: INPUTS for OBSCUMSR4 and ADJCUMSR4*******/

local j=`yr1'
while `j'<=`yr2'{
gen double mytemp3`j'=0
gen double mytemp4`j'=0

	local v=1+(`j'-`yr1')
	while `v'<=(80-`dif')+(`j'-`yr1'){
	summarize destx_`j' if _n>=`v'&_n<=80+(`j'-`yr1')
	replace mytemp3`j'=r(sum) if _n==`v'
	summarize ddx_`j' if _n>=`v'&_n<=80+(`j'-`yr1')
	replace mytemp4`j'=r(sum) if _n==`v'

local v=`v'+1
	}
local j=`j'+1
}

local j=`yr1'
local h=0
while `j'<=`yr2'{
replace adjcohort_dx=adjcohort_dx + mytemp1`j'[_n+`h'] 
replace obscohort_dx=obscohort_dx + mytemp2`j'[_n+`h'] 
replace newadjcohort_dx=newadjcohort_dx + mytemp3`j'[_n+`h'] 
replace newobscohort_dx=newobscohort_dx + mytemp4`j'[_n+`h'] 


local h=`h'+1
local j=`j'+1
}


				/*Computes Cumulated populations for the two censues*/

gen double adjpop1=sum(popest1x)
gen double adjpop2=sum(popest2x)
gen double obspop1=sum(No1x)
gen double obspop2=sum(No2x)

scalar aa=obspop1[_N]
scalar bb=obspop2[_N]
scalar cc=adjpop1[_N]
scalar ddd=adjpop2[_N]

gen double obscumpop1=aa-obspop1[_n-1] if _n~=1
gen double obscumpop2=bb-obspop2[_n-1] if _n~=1
gen double adjcumpop1=cc-adjpop1[_n-1] if _n~=1
gen double adjcumpop2=ddd-adjpop2[_n-1] if _n~=1


/*using only quantities that EXCLUDE open age group....this will depend on length of intercensal period*/

gen newadjcumpop2=adjcumpop2-popest2x[81]
gen newadjcumpop1=adjcumpop1-adjcumpop1[81-`dif']
gen newobscumpop1=obscumpop1-obscumpop1[81-`dif']
gen newobscumpop2=obscumpop2-obscumpop2[81]

				/*Calculate cum ratios adjusting for estimated completeness factors*/

				/*Remember that h= C2/C1 and j=C3/(.5*(C1+C2))*/

gen double adjcumsr1=.			/*standard cumsr adjusted for everything*/
gen double adjcumsr2=.			/*standard cumsr adjusted only for completeness*/
gen double adjcumsr3=.			/*OBSERVED standard cumsr*/
gen double obscumsr4=.			/*OBSERVED modified cumsr*/ 
gen double adjcumsr4a=.			/*non standard cumsr adjusted for BH*/
gen double adjcumsr4b=.			/*non standard cumsr adjusted for everything*/


/*adjusted for tetas and  BH using adjusted r*/

replace adjcumsr1= ((1/`adjust1')*(adjcumpop2[_n+`dif']/adjcumpop1))/(1-((adjcohort_dx/adjcumpop1)*(1/`adjust2')*(2/(`adjust1'+1)))) 

/*adjusted for BH only (using adjusted r)*/

replace adjcumsr2=((1/`adjust1')*(obscumpop2[_n+`dif']/obscumpop1))/(1-((obscohort_dx/obscumpop1)*(1/`adjust2')*(2/(`adjust1'+1))))

/*observed cumsr*/

replace adjcumsr3= (obscumpop2[_n+`dif']/obscumpop1)/(1-obscohort_dx/obscumpop1)

/*observed cumsr up to age 84*/

replace obscumsr4= (newobscumpop2[_n+`dif']/newobscumpop1)/(1-newobscohort_dx/newobscumpop1)

/*adjusted for BH only (using adjusted r) cumsr up to age 84*/

replace adjcumsr4a=((1/`adjust1')*(newobscumpop2[_n+`dif']/newobscumpop1))/(1-((newobscohort_dx/newobscumpop1)*(1/`adjust2')*(2/(`adjust1'+1)))) 

/*adjusted for tetas and BH using adjusted r cumsr up to age 84*/

replace adjcumsr4b=((1/`adjust1')*(newadjcumpop2[_n+`dif']/newadjcumpop1))/(1-((newadjcohort_dx/newadjcumpop1)*(1/`adjust2')*(2/(`adjust1'+1)))) 



						/*ESTIMATION OF ADJUSTED LIFE TABLES from age 5 to age 85*/

local j=1
while `j'<=2 {


gen adjQx`j'=adjMx`j'/(1 +.5*adjMx`j')
replace adjQx`j'=1 if _n==81

gen adjlx`j'=0
replace adjlx`j'=100000 if _n==1
replace adjlx`j'=adjlx`j'[_n-1]*(1-adjQx`j'[_n-1]) if _n>1 & _n<=81   
gen adjLx`j'=.5*(adjlx`j'+adjlx`j'[_n+1]) if _n<=80 
replace adjLx`j'= adjlx`j'/ adjMx`j' if _n==81
*replace adjLx`j'=0 if _n==81   


gen aux`j'Tx=sum(adjLx`j')
gen adjTx`j'=0
replace adjTx`j'=aux`j'Tx[_N] if _n==1
replace adjTx`j'=aux`j'Tx[_N]-aux`j'Tx[_n-1] if _n>=2
gen adjEx`j'=adjTx`j'/adjlx`j'


local j=`j'+1


}


				/*ESTIMATION OF OBSERVED LIFE TABLE FROM AGE 5 to AGE 85*/

gen obslx=0
gen obsQx=obsMx/(1 +.5*obsMx)
replace obsQx=1 if _n==81

replace obslx=100000 if _n==1
replace obslx=obslx[_n-1]*(1-obsQx[_n-1]) if _n>1 & _n<=81
gen obsLx=.5*(obslx+obslx[_n+1]) if _n<=80
replace obsLx=obslx/obsMx if _n==81

gen aux4Tx=sum(obsLx)
gen obsTx=0
replace obsTx=aux4Tx[_N] if _n==1
replace obsTx=aux4Tx[_N]-aux4Tx[_n-1] if _n>=2
gen obsEx=obsTx/obslx


gen teta1=`teta1'
gen teta3=`teta3'
gen ctry=`1'
gen year=`yr1'
gen brass2=`2'
gen bh1Adj=`3'
gen bh1=`11'
gen sex=`6'
gen year2=`yr2'
gen myear=`k'
gen run=`13'

format adjcumsr1  adjcumsr2 adjcumsr3 obsMx adjMx1 adjMx2 obsQx adjQx1 adjQx2 %6.4f
format obsEx adjEx2 adjEx1 %5.2f

save recovered12.dta, replace			/*this is final recovered data*/

merge 1:1 age using mytemp

keep ctry myear year age sex teta1 teta3 adjcumsr1 adjcumsr2 adjcumsr3 obscumsr4 adjcumsr4a adjcumsr4b obsMx obsQx obslx obsLx obsTx obsEx /*
*/ adjMx1 adjQx1 adjlx1 adjLx1 adjTx1 adjEx1 adjMx2 adjQx2 adjlx2 adjLx2 adjTx2 adjEx2 brass2 bh1Adj run


*saveold CLEANfinallymatrixCreal_`13'.dta, replace

*line adjcumsr1 age if age>45 & (adjcumsr1>0 & adjcumsr1<5) , title("`1';`6';`k';`4';`5'") yline(1) 
*graph save cumsr_`13', replace
*graph export cumsr_`13'.emf, replace

keep ctry myear sex age teta1 teta3 adjcumsr1 adjcumsr2 adjcumsr3 obscumsr4 adjcumsr4a adjcumsr4b adjMx1 adjEx1 obsMx obsEx run
order ctry myear sex age teta1 teta3 adjcumsr1 adjcumsr2 adjcumsr3 obscumsr4 adjcumsr4a adjcumsr4b adjMx1 adjEx1 obsMx obsEx run

save CLEANcumsr_`1'_`8'_`6'_`13'.dta, replace 

end

/********************************/

quietly CLEANfinallymatrixC_thetas