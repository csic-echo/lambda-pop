clear
program drop _all
drop _all

/****************************************************************************************/
program define CLEANfinallymatrix
set more off


/*

MAY 2018

IMPORTANT: CURRENT VERSION IS BUILT TO HANDLE RECOVERY DATA FROM COUNTRIES
THE PROGRAM APPLIES TWO METHODS TO HANDLE OPEN AGE GROUP 85+ THESE METHODS ARE
IN THE SUBROUTINES

		FINALLYMATRIX1
		FINALLYMATRIX2

MAY 2014!!!!!!!! FINALLYMATRIX2 was judged to be too sensitive and was abandoned

THIS PROGRAM IS THE SAME AS ORIGINAL PROGRAM CLEANfinallymatrix.do but set up to
recover a distorted population that has an open age group at 85+ . This is 
done using ONLY ONE OF TWO methods. 

Method USE is FINALLYMATRIX1 

Method consist of using matrices for ages 5 to 85+ (81 age groups)
Calculates an auxiliar function ADJCUMSR4 which should be correct
when there is 85+ open age group

Run by invoking finallymatrix1


THE SECOND METHOD (FINALLYMATRIX2.DO) IS INCLUDED IN CLEANfinallymatrixC.do

******************************************************************************************
*****************************************************************************************/

local ctry=1

  local c1=0.8
  while `c1'<=1  { 

   local c2=0.8
   while `c2'<=1  { 

      local c3=0.70
      while `c3'<=1  {	 

          local teta1=0
          while `teta1'<=2.5  { 

	       		local teta3=0
		      	while `teta3'<=2.5  { 

finallymatrix1 `ctry' `c1' `c2' `c3' `teta1' `teta3' 1980 1 1990 1  
local ctry=`ctry' + 1

					local teta3=`teta3'+0.50
						              }

			local teta1=`teta1'+0.50
				              }

         local c3= `c3'+ 0.05
		             }

    local c2=`c2'+ 0.05
                   }

  local c1=`c1'+ 0.05
   
  }

/***************/

use CLEANfinallymatrix_st_1, clear
local i=2
 while `i' <=6300  {
append using CLEANfinallymatrix_st_`i' 

local i=`i'+1
}

save CLEANfinallymatrixS0s_st, replace    /*S=south, 0=constant scenario, old age pattern, s=single ages*/


end
    
/****************************************************************************************/
/****************************************************************************************/
program define finallymatrix1

/* This is main program

LAST WRITTEN ON OCTOBER 2011 PROGRAM IS SET TO RUN TO PRODUCE FINAL ADJUSTED VALUES OF 
MORTALITY RATES AS WELL AS ADJUSTED LIFE TABLES WHERE THE ADJUSTMENT INCLUDE COMPLETENESS
AND AGE MISSTATEMENT


1) this program reads two censuses and intercensal deaths, calculates
and adjusted populations and deaths after adjusting 

a. Needs parameters of age overstatement, teta1 and teta3
b. Needs parameters of completeness of census and death registration

2) Takes the values of teta1, teta3, and 
the estimates of completeness to generate adjusted values of Mx and
then calls a life table program to generate the life table.

3) Essentially what the program does is to UNDO what the program CLEANsimulationmatrix.do
does to introduce distortions

4) The last part of the rogram calculates life tables by single years of age using
adjusted and unadjusted Mx's

The syntax is 

		finallymatrix1 c h j t1 t2 g x y1 m1 y2 m2 jj

	1	c is country code
	2	h is C2/C1 from brass
	3	j is C3/(.5*(C1+C2)) from BH adjusted for rate of growth
	4  	t1 is teta1
	5	t3 is teta3
	6	g is 1 if male and 2 if females
	7	x is name of file (structure identical to the files used by REAL-ESTIMATION.DO
	8	y1 is the first year of census
	9	m1 is the month of first census
	10	y2 is the year of second census
	11	m2 is the month of the second census
	12	similar to 2 but not adjusted by rate of growth
	13	counter
*/

use CLEANsimmatrixS0s_st, clear
drop if age<5										/*RENAMES locals */

*rename comp1 c1
*rename comp2 c2
*rename comp3 c3

local ctry=`1'
local c1=`2'
local c2=`3'
local c3=`4'
local teta1=`5'
local teta3=`6'
local yr1=`7'
local mo1=`8'
local yr2=`9'
local mo2=`10'
local adjust1=`3'/`2'
local adjust2=`4'/(0.5*(`2'+`3'))
*local run=`11' 
local cm2=12*(`yr2'-1900)+(`mo2'-1) 				/*century month*/
local cm1=12*(`yr1'-1900)+(`mo1'-1) 				/*century month*/
scalar exposure=(`cm2'-`cm1')/12				/*in years*/
gen year=`yr1'
local dif= `yr2'-`yr1'							/*length of intercensal period*/
local aux=`yr2'-1	/*local used to count only deaths up to and including the entire year before census No2:OJO must be changed in real cases*/

local k=trunc((`yr1'+`yr2')/2) 
keep if ctry==`1' 

keep ctry age /*c1 c2 c3 teta1 teta3*/ year pf`yr1' pf`yr2' df`yr1'-df`aux' cohort_dox cohort_Dx inter_Dx inter_dox
gen No1x=pf`yr1'
gen No2x=pf`yr2'
rename pf`yr1' px_`yr1'
rename pf`yr2' px_`yr2'

	local h=`yr1'
	while `h'<=`yr2'-1 {
	gen ddx_`h'=df`h'
	rename df`h' dx_`h' 
	local h=`h'+1
	}

save mytemp.dta, replace
								/*Basic functions*/

/*

The task is to take the estimated teta1, test3, and C2/C1 as well as C3/(.5*(C1+C2)
and calculate adjusted expected/observed population ratios as well as death rates. These
wil be corrected for both completeness and age overstatement

Fot this what matters is the following:

A. Age misreporting :

mu_x= standard proportion who overstate at age x; 
nu_x= standard proportion who overstate ages at death;

Because the work we know of only deals with over statement 
of populations we will assume that mu_x=nu_x

We can increase (decrease) mu_x and nu_x by multiplying by a constant;
remember though that these are proportions and should not exceed 1.0

teta1= factor to modify mu_x  
teta3= factor to modify nu_x

alphaj=the standard pattern of overstatement  (has values for j=1,2,...10)
alphaj is the prob that if somebody overstates age it does so by j years

k=the max Number of years of overstatement throughout is fixed at 10 years

C1 and C2 = completeness of censuses at time 1 and time 2
C3= completeness of death registration

B. Patterns of age ovestatement

The patterns of ovestatement by years of exaggeration or reduction 
are defined by alpha(j) 

   alpha(j)=prob de exagerar j a�os,una ecuacion que es funcion de j (pop)
   
For a good explanation of procedure to get alphaj and betaj see program 
CR_agemistatement.do. Here is a summary:

We use the probs estimated by Brenes with a multinomial (for j=1,...10) model 
and as function of age. We produce predicted values for each age of the probs of 
exagerar (disminuir) by j years. 

For age 10 the value reprpesents the prob of exagerate or diminish by 10+. 
To correct for this we assume that prob of  exagerate or diminish goes down 
to 0 at age 15 and estimate prob of exagerate or diminish as PR(10+)*(2/5). 

For each j we now calculate the MEDIAN value  of the predicted probabilities 
produced across ages. We thus have 10 values of probs of exagerating (diminishing), 
one for each number of year of exageration. Call these MD(j).  WE then estimate the 
relation ln(MD(j)/(1-MD(j))= a+b ln(j) and produce predicted values for each j. Finally, 
we add them all up, obtain the sum and standardize the predicted values by this sum 
to make sure that the addition of the probs adds up to 1.0. We do the same for both 
exageration and underestimation.

The values of alphaj and beta j used below are these final results and can be found in 
the file CR_FINAL_ERRORPATTERNS.DTA

local alpha1= .6205334   
local alpha2= .1964748           
local alpha3= .0793082           
local alpha4= .0397628           
local alpha5= .0229676           
local alpha6= .0145954           
local alpha7= .0099264           
local alpha8= .0071005           
local alpha9=  .0052806           
local alpha10= .0040503           

*/


/*Defines quantities for the transition matrix that produces distortions of ages*/


gen muxpop=0
gen muxmort=0
replace muxpop=exp(-2.127674+.0136934*age)/(1+ exp(-2.127674+.0136934*age)) if age>=45

/*important change to deal with open age group: prob of overstating if in 85+ is ZERO*/

replace muxpop=0 if age>=85

replace muxmort=muxpop if age>=45


/*
we alter standard values of mux to create different levels of
over statement of population age and of death ages
*/

replace muxpop=`teta1'*muxpop if age>=45    /*proportion overstating ages above 45 only*/
replace muxpop=0 if age==85
replace muxmort=`teta3'*muxmort if age>=45  /*proportion overstating ages above 45 only*/
replace muxmort=0 if age==85

local alpha1= .6205334   
local alpha2= .1964748           
local alpha3= .0793082           
local alpha4= .0397628           
local alpha5= .0229676           
local alpha6= .0145954           
local alpha7= .0099264           
local alpha8= .0071005           
local alpha9=  .0052806           
local alpha10= .0040503          

 

/************************BEGINS RECOVERY******************/


/************************IMPORTANT*************************

In cases of simulated populations we use 0-85 and matrix must be of n=86
In cases of observed countries we use 5-85 and matrix must be of n=81

*********************************************************/

/*

generates a data set having 81 columns in the following form

	1-O1	0	0	0....
	O1	1-O2	0	0....
	O1	O2	1-O3	0...


for both population and mortality

*/


local g=1
while `g'<=81 {						/****:for real pops should be 81 (5-85)***/
gen muxpop`g'=1
local g=`g'+1
}

local g=1
while `g'<=81 {
replace muxpop`g'=muxpop[`g']
replace muxpop`g'=1-muxpop if _n==`g'
replace muxpop`g'=0 if _n<`g'

local g=`g'+1
}

save muxpop.dta, replace

local g=1
while `g'<=81 {
gen muxmort`g'=1
local g=`g'+1
}

local g=1
while `g'<=81 {
replace muxmort`g'=muxmort[`g']
replace muxmort`g'=1-muxmort if _n==`g'
replace muxmort`g'=0 if _n<`g'
local g=`g'+1
}

save muxmort.dta, replace

/*
generates a data set having 81 columns in the following form

	1	0	0	0....
	alpha1	1	0	0....
	alpha2	alpha1	1	0...
	alpha3	alpha2  alpha1	1....
	alpha4	alpha3	alpha2	alpha1	1

*/

local j=1
while `j'<=81 {
gen ALPHA`j'=0
local j=`j'+1
}

local j=1
while `j'<=81{
replace ALPHA`j'= 0 if _n<`j'
replace ALPHA`j'=1 if `j'==_n

	local r=1
	while `r'<=10{
	replace ALPHA`j'= `alpha`r'' if _n-`j'==`r' & `r'<=10
	local r=`r'+1
	}

local j=`j'+1
}

save alpha.dta, replace

/**************************************************************************
we now change the alpha's for ages 76-84 so that the last values is the sum
of alphas's that will not be used when the last age group is 85+:

for age 76 the last alpha must be alpha9+alpha10
for age 77 the last alpha must be alpha8+alpha9+alpha10
......
for age 84 the last alpha must be alpha2+......+alpha10

**************************************************************************/

local newalpha1=`alpha10'+`alpha9'
local newalpha2=`newalpha1'+`alpha8'
local newalpha3=`newalpha2'+`alpha7'
local newalpha4=`newalpha3'+`alpha6'
local newalpha5=`newalpha4'+`alpha5'
local newalpha6=`newalpha5'+`alpha4'
local newalpha7=`newalpha6'+`alpha3'
local newalpha8=`newalpha7'+`alpha2'
local newalpha9=`newalpha8'+`alpha1'

local z=72
while `z'<=80 {
local v=`z'-71
replace ALPHA`z'=`newalpha`v'' if _n==81
local z=`z'+1
}


save alpha.dta, replace


/*
we have the MU's and the ALPHA's: we now multiply them together
to get main transtion matrix that distorts bth populations and deaths
*/

local f=1
while `f'<=81 {
gen mainpop`f'=muxpop`f'*ALPHA`f'
local f=`f'+1
}

local f=1
while `f'<=81 {
gen mainmort`f'=muxmort`f'*ALPHA`f'
local f=`f'+1
}

order age mainpop* mainmort*

save transition.dta, replace

mkmat mainpop1-mainpop81, matrix(TRANSITIONPOP)           
mkmat mainmort1-mainmort81, matrix(TRANSITIONMORT)

/*use inverse of transition matrices to recover populations and deaths*/


/*for census No 1*/

mkmat No1x, matrix(POPOBS1)
matrix POPEST1=inv(TRANSITIONPOP)*POPOBS1
matrix POP1=POPOBS1, POPEST1

preserve
keep age   

mat colnames POP1 = No1x popest1x
svmat double POP1, names(col)
sort age
save POP1est.dta, replace

restore

/*for census No 2*/

mkmat No2x, matrix(POPOBS2)
matrix POPEST2=inv(TRANSITIONPOP)*POPOBS2
matrix POP2=POPOBS2, POPEST2

preserve

keep age  

mat colnames POP2 = No2x popest2x
svmat double POP2, names(col)
sort age
save POP2est.dta, replace

restore

/*Deaths: adds intercensal deaths OJO: in observed cases loop must include deaths in year of second census*/

local j=`yr1'
while `j'<=`aux' {
mkmat ddx_`j', matrix(DDX`j')
matrix DESTX`j'=inv(TRANSITIONMORT)*DDX`j'
local j=`j'+1
}

preserve
keep age

local g=`yr1'
while `g'<=`aux' {
matrix MORT`g'=DDX`g', DESTX`g'
mat colnames MORT`g'=ddx_`g' destx_`g'
svmat double MORT`g', names(col)
sort age
save MORTest`g'.dta, replace
local g=`g'+1
}

								/*we now put together all the data*/
use POP1est
sort age
merge 1:1 age using POP2est, generate(_merge`yr1')
sort age

local h=`yr1'+1
while `h'<=`aux' {
merge 1:1 age using MORTest`h', generate(_merge`h')
local h=`h'+1
}

save recovered.dta, replace		/*this is the initial recovered data*/

clear matrix

!erase MORT*.dta
!erase POP*.dta
!erase mux*.dta
!erase transition.dta
!erase alpha.dta
									/*END OF RECOVERY*/
									/*Computation of intercensal deaths*/

gen obsintercensal=0
gen adjintercensal=0

replace obsintercensal=((12-(`mo1'-1))/12)* ddx_`yr1'+ ((`mo2'-1)/12)*ddx_`aux' 
replace adjintercensal=((12-(`mo1'-1))/12)* destx_`yr1'+ ((`mo2'-1)/12)*destx_`aux'

local t=`yr1'+1
while `t'<=`aux' {                             /*********check this **************/	 
replace obsintercensal=obsintercensal+ddx_`t'
replace adjintercensal=adjintercensal+destx_`t'
local t=`t'+1
}

		/*calculates adjusted death rates for intercensal period*/

gen double adjMx1=(adjintercensal/(exposure*.5*(popest1x+popest2x)))*(1/`adjust2')  /*adjusted for everything with BH*/
gen double adjMx2=(obsintercensal/(exposure*.5*(No1x+No2x)))*(1/`adjust2') 			/*adjusted only for completeness with BH*/
gen double obsMx=(obsintercensal/(exposure*.5*(No1x+No2x)))							/*not adjusted at all=observed*/

/*CALCULATE RATIOS OBSERVED TO EXPECTED BEFORE and AFTER ADJUSTING FOR AGE MISSTATEMENT AND COMPLETENESS*/


		/*Calculates deaths at ages x+ during intercensal period*/


gen double adjcohort_dx=0
gen double obscohort_dx=0
gen double newadjcohort_dx=0
gen double newobscohort_dx=0




replace destx_`yr1'=destx_`yr1'*((12-(`mo1'-1))/12)	/*census 1: EXCLUDE fraction of deaths in first part of first year*/ 
replace destx_`aux'=destx_`aux'*((`mo2'-1)/12)		/*census 2: INCLUDE ONLY fraction of deaths in first part of second year*/

replace ddx_`yr1'=ddx_`yr1'*((12-(`mo1'-1))/12) 			 
replace ddx_`aux'=ddx_`aux'*((`mo2'-1)/12)

/***********************IGNORING THAT THERE IS AN open age group at 85+*******************************/

local j=`yr1'
while `j'<=`yr2'-1 {
gen double mytemp1`j'=0
gen double mytemp2`j'=0
	local v=1+(`j'-`yr1')
	while `v'<=81 {
	summarize destx_`j' if _n>=`v'
	replace mytemp1`j'=r(N)*r(mean) if _n==`v'
	summarize ddx_`j' if _n>=`v'
	replace mytemp2`j'=r(N)*r(mean) if _n==`v'
	local v=`v'+1
	}
local j=`j'+1
}


/*************NOT IGNORING age group at 85: INPUTS for OBSCUMSR4 and ADJCUMSR4*******/

local j=`yr1'
while `j'<=`yr2'-1 {
gen double mytemp3`j'=0
gen double mytemp4`j'=0

	local v=1+(`j'-`yr1')
	while `v'<=(80-`dif')+(`j'-`yr1'){
	summarize destx_`j' if _n>=`v'&_n<=80+(`j'-`yr1')
	replace mytemp3`j'=r(sum) if _n==`v'
	summarize ddx_`j' if _n>=`v'&_n<=80+(`j'-`yr1')
	replace mytemp4`j'=r(sum) if _n==`v'

local v=`v'+1
	}
local j=`j'+1
}

local j=`yr1'
local h=0
while `j'<=`yr2'-1 {
replace adjcohort_dx=adjcohort_dx + mytemp1`j'[_n+`h'] 
replace obscohort_dx=obscohort_dx + mytemp2`j'[_n+`h'] 
replace newadjcohort_dx=newadjcohort_dx + mytemp3`j'[_n+`h'] 
replace newobscohort_dx=newobscohort_dx + mytemp4`j'[_n+`h'] 


local h=`h'+1
local j=`j'+1
}


				/*Computes Cumulated populations for the two censues*/

gen double adjpop1=sum(popest1x)
gen double adjpop2=sum(popest2x)
gen double obspop1=sum(No1x)
gen double obspop2=sum(No2x)

scalar aa=obspop1[_N]
scalar bb=obspop2[_N]
scalar cc=adjpop1[_N]
scalar ddd=adjpop2[_N]

gen double obscumpop1=aa-obspop1[_n-1] if _n~=1
gen double obscumpop2=bb-obspop2[_n-1] if _n~=1
gen double adjcumpop1=cc-adjpop1[_n-1] if _n~=1
gen double adjcumpop2=ddd-adjpop2[_n-1] if _n~=1


/*using only quantities that EXCLUDE open age group....this will depend on length of intercensal period*/

gen newadjcumpop2=adjcumpop2-popest2x[81]
gen newadjcumpop1=adjcumpop1-adjcumpop1[81-`dif']
gen newobscumpop1=obscumpop1-obscumpop1[81-`dif']
gen newobscumpop2=obscumpop2-obscumpop2[81]

				/*Calculate cum ratios adjusting for estimated completeness factors*/

				/*Remember that h= C2/C1 and j=C3/(.5*(C1+C2))*/

gen double adjcumsr1=.			/*standard cumsr adjusted for everything*/
gen double adjcumsr2=.			/*standard cumsr adjusted only for completeness*/
gen double adjcumsr3=.			/*OBSERVED standard cumsr*/
gen double obscumsr4=.			/*OBSERVED modified cumsr*/ 
gen double adjcumsr4a=.			/*non standard cumsr adjusted for BH*/
gen double adjcumsr4b=.			/*non standard cumsr adjusted for everything*/


/*adjusted for tetas and  BH using adjusted r*/
replace adjcumsr1= ((1/`adjust1')*(adjcumpop2[_n+`dif']/adjcumpop1))/(1-((adjcohort_dx/adjcumpop1)*(1/`adjust2')*(2/(`adjust1'+1)))) 

/*adjusted for BH only (using adjusted r)*/
replace adjcumsr2=((1/`adjust1')*(obscumpop2[_n+`dif']/obscumpop1))/(1-((obscohort_dx/obscumpop1)*(1/`adjust2')*(2/(`adjust1'+1))))

/*observed cumsr*/
replace adjcumsr3= (obscumpop2[_n+`dif']/obscumpop1)/(1-obscohort_dx/obscumpop1)

/*observed cumsr up to age 84*/
replace obscumsr4= (newobscumpop2[_n+`dif']/newobscumpop1)/(1-newobscohort_dx/newobscumpop1)

/*adjusted for BH only (using adjusted r) cumsr up to age 84*/
replace adjcumsr4a=((1/`adjust1')*(newobscumpop2[_n+`dif']/newobscumpop1))/(1-((newobscohort_dx/newobscumpop1)*(1/`adjust2')*(2/(`adjust1'+1)))) 

/*adjusted for tetas and BH using adjusted r cumsr up to age 84*/
replace adjcumsr4b=((1/`adjust1')*(newadjcumpop2[_n+`dif']/newadjcumpop1))/(1-((newadjcohort_dx/newadjcumpop1)*(1/`adjust2')*(2/(`adjust1'+1)))) 



						/*ESTIMATION OF ADJUSTED LIFE TABLES from age 5 to age 85*/

local j=1
while `j'<=2 {

gen adjQx`j'=adjMx`j'/(1 +.5*adjMx`j')
replace adjQx`j'=1 if _n==81

gen adjlx`j'=0
replace adjlx`j'=100000 if _n==1
replace adjlx`j'=adjlx`j'[_n-1]*(1-adjQx`j'[_n-1]) if _n>1 & _n<=81   
gen adjLx`j'=.5*(adjlx`j'+adjlx`j'[_n+1]) if _n<=80 
replace adjLx`j'= adjlx`j'/ adjMx`j' if _n==81
*replace adjLx`j'=0 if _n==81   

gen aux`j'Tx=sum(adjLx`j')
gen adjTx`j'=0
replace adjTx`j'=aux`j'Tx[_N] if _n==1
replace adjTx`j'=aux`j'Tx[_N]-aux`j'Tx[_n-1] if _n>=2
gen adjEx`j'=adjTx`j'/adjlx`j'


local j=`j'+1
}


				/*ESTIMATION OF OBSERVED LIFE TABLE FROM AGE 5 to AGE 85*/

gen obslx=0
gen obsQx=obsMx/(1 +.5*obsMx)
replace obsQx=1 if _n==81

replace obslx=100000 if _n==1
replace obslx=obslx[_n-1]*(1-obsQx[_n-1]) if _n>1 & _n<=81
gen obsLx=.5*(obslx+obslx[_n+1]) if _n<=80
replace obsLx=obslx/obsMx if _n==81

gen aux4Tx=sum(obsLx)
gen obsTx=0
replace obsTx=aux4Tx[_N] if _n==1
replace obsTx=aux4Tx[_N]-aux4Tx[_n-1] if _n>=2
gen obsEx=obsTx/obslx


gen myear=`k'
gen Cc=`adjust1'
gen Cm=`adjust2'
gen c1=`c1'		/*note that these simply define all these as float but that stata stores them as double*/
gen c2=`c2'
gen c3=`c3'
gen teta1=`teta1'
gen teta3=`teta3'

save recovered12.dta, replace			/*this is final recovered data*/

merge 1:1 age using mytemp

l ctry age teta1 teta3 inter_Dx  

				/*ESTIMATION OF TRUE LIFE TABLE*/

gen trueMx= (inter_Dx/(exposure*.5*(px_1980+px_1990)))	

gen trueQx=trueMx/(1 +.5*trueMx)
replace trueQx=1 if _n==101

gen truelx=100000 if _n==1
replace truelx=truelx[_n-1]*(1-trueQx[_n-1]) if _n>1 & _n<=101
gen trueLx=.5*(truelx+truelx[_n+1]) if _n<=100
replace trueLx=truelx/trueMx if _n==101

gen aux5Tx=sum(trueLx)
gen trueTx=0
replace trueTx=aux5Tx[_N] if _n==1
replace trueTx=aux5Tx[_N]-aux5Tx[_n-1] if _n>=2
gen trueEx=trueTx/truelx

keep ctry myear year age teta1 teta3 c1 c2 c3 adjcumsr1 adjcumsr2 adjcumsr3 obscumsr4 obsMx obsQx obslx obsLx obsTx obsEx /*
*/ adjMx1 adjQx1 adjlx1 adjLx1 adjTx1 adjEx1 adjMx2 adjQx2 adjlx2 adjLx2 adjTx2 adjEx2 trueMx trueQx truelx trueLx trueTx trueEx Cc Cm 

order ctry myear year age teta1 teta3 c1 c2 c3 adjcumsr1 adjcumsr2 adjcumsr3 obscumsr4 obsMx obsQx obslx obsLx obsTx obsEx /*
*/ adjMx1 adjQx1 adjlx1 adjLx1 adjTx1 adjEx1 adjMx2 adjQx2 adjlx2 adjLx2 adjTx2 adjEx2 trueMx trueQx truelx trueLx trueTx trueEx Cc Cm 

save CLEANfinallymatrix_st_`1', replace

end

/********************************/

quietly CLEANfinallymatrix

