drop _all
program drop _all

program define reg1_betas
set more off

/*THIS PROGRAM IS DESIGNED TO ONLY DO REGRESSION WITH INPUT CLEANsim4s_all (FROM CLEANsimulation_4.do). It use simulated
simulated populations for diferent velues of teta1 and teta3, and regresses 1/cumsr against  teta1 and teta3 for each age X*/

/*
/****************************/
/**  Case 1: teta1 > teta3 **/
/****************************/
use CLEANsimmatrix_ST

drop if age<45 | age>75		/* eliminates negative cumsr_ values */ 	

keep if (teta3 < teta1) & (comp1==1 & comp2==1 & comp3==1) & cumsr>0 

*keep if (teta3 > teta1) & (comp1==float(.90) & comp3==float(.90) & comp3==float(.90))

gen coeff_const=.
gen coeff_teta1=.
gen coeff_teta3=.
gen rsquare=.
gen icumsr=1/cumsr 

local i=45 
while `i'<76 { 

reg icumsr teta1 teta3 if age==`i'  
matrix beta_`i'=e(b)
svmat beta_`i'
local rsquare_`i'=e(r2)

replace coeff_const=beta_`i'[1,3] if age==`i'
replace coeff_teta1=beta_`i'[1,1] if age==`i'
replace coeff_teta3=beta_`i'[1,2] if age==`i'
replace rsquare=e(r2) if age==`i'

drop beta_`i'1 beta_`i'2 beta_`i'3  

local i=`i'+1
}

rename coeff_const alpha 
rename coeff_teta1 beta1
rename coeff_teta3 beta3

collapse (max) alpha beta1 beta3 rsquare, by(age)
format alpha - rsquare %7.6f 

sort age
save coeffC1_betas, replace     /* saves just coefficients of the regression and the Rsq */

/****************************/
/** Case 2: teta3 > teta1 **/
/****************************/
clear
use CLEANsimmatrix_ST  

drop if age<45 | age>75		/* eliminates negative cumsr_ values */ 	

keep if (teta3 > teta1) & (comp1==1 & comp2==1 & comp3==1) & cumsr>0
*keep if (teta3 > teta1) & (comp1==float(.90) & comp3==float(.90) & comp3==float(.90))

gen coeff_const=.
gen coeff_teta1=.
gen coeff_teta3=.
gen rsquare=.
gen icumsr=1/cumsr 

local i=45 
while `i'<76 { 

reg icumsr teta1 teta3 if age==`i'    

matrix beta_`i'=e(b)
svmat beta_`i'
local rsquare_`i'=e(r2)

replace coeff_const=beta_`i'[1,3] if age==`i'
replace coeff_teta1=beta_`i'[1,1] if age==`i'
replace coeff_teta3=beta_`i'[1,2] if age==`i'
replace rsquare=e(r2) if age==`i'

drop beta_`i'1 beta_`i'2 beta_`i'3  

local i=`i'+1
}

rename coeff_const alpha 
rename coeff_teta1 beta1
rename coeff_teta3 beta3

collapse (max) alpha beta1 beta3 rsquare, by(age)
format alpha - rsquare %7.6f 

sort age
save coeffC2_betas, replace     /* saves just coefficients of the regression and the Rsq */


/****************************/
/** Case 3: teta3 = teta1 **/
/****************************/
clear
use CLEANsimmatrix_ST  

drop if age<45 | age>75		/* eliminates negative cumsr_ values */ 	

keep if (teta3 == teta1) & (comp1==1 & comp2==1 & comp3==1) & cumsr>0
*keep if (teta3 > teta1) & (comp1==float(.90) & comp3==float(.90) & comp3==float(.90))

gen coeff_const=.
gen coeff_teta1=.
gen coeff_teta3=.
gen rsquare=.
gen icumsr=1/cumsr 

local i=45 
while `i'<76 { 

reg icumsr teta1 teta3 if age==`i'   

matrix beta_`i'=e(b)
svmat beta_`i'
local rsquare_`i'=e(r2)

replace coeff_const=beta_`i'[1,3] if age==`i'
replace coeff_teta1=beta_`i'[1,1] if age==`i'
replace coeff_teta3=beta_`i'[1,2] if age==`i'
replace rsquare=e(r2) if age==`i'

drop beta_`i'1 beta_`i'2 beta_`i'3  

local i=`i'+1
}

rename coeff_const alpha 
rename coeff_teta1 beta1
rename coeff_teta3 beta3

collapse (max) alpha beta1 beta3 rsquare, by(age)
format alpha - rsquare %7.6f 

sort age
save coeffC3_betas, replace     /* saves just coefficients of the regression and the Rsq */
*/

/****************************/
/** Case 4: all thetas **/
/****************************/
clear
use CLEANsimmatrix_ST  

drop if age<45 | age>75		/* eliminates negative cumsr_ values */ 	

keep if (teta3 != teta1) & (comp1==1 & comp2==1 & comp3==1) & cumsr>0
*keep if (teta3 > teta1) & (comp1==float(.90) & comp3==float(.90) & comp3==float(.90))

gen coeff_const=.
gen coeff_teta1=.
gen coeff_teta3=.
gen rsquare=.
gen icumsr=1/cumsr 

local i=45 
while `i'<76 { 

reg icumsr teta1 teta3 if age==`i'   

matrix beta_`i'=e(b)
svmat beta_`i'
local rsquare_`i'=e(r2)

replace coeff_const=beta_`i'[1,3] if age==`i'
replace coeff_teta1=beta_`i'[1,1] if age==`i'
replace coeff_teta3=beta_`i'[1,2] if age==`i'
replace rsquare=e(r2) if age==`i'

drop beta_`i'1 beta_`i'2 beta_`i'3  

local i=`i'+1
}

rename coeff_const alpha 
rename coeff_teta1 beta1
rename coeff_teta3 beta3

collapse (max) alpha beta1 beta3 rsquare, by(age)
format alpha - rsquare %7.6f 

sort age
save coeffC4_betas, replace     /* saves just coefficients of the regression and the Rsq */


end
reg1_betas