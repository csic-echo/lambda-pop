program drop _all
program define prepare

/*this program reads in a data file containing age (0,1,5,10,,,75) and the 
coefficients used by Coale_Demeny to produce the various Q values according
to the linear equation and the logarithmic equation (c1, c2, c3 and c4).

The program determines the two values of E(10) at which the regression lines 
cross and writes out a file with age, c1, c2, c3, c4, e1 and e2
*/

infile using CD_coefficients


/*determine the two functions, q1 and q2, create the difference, g=q1-q2
and then seek maximum value of g by solving for g'=0 where g' is first
derivative of g. We then use this max to guide us in identifying whether
a given value of E(10) is closer to the first crossing than to the second
crossing
*/

gen eten1=0
gen eten2=0
gen max=0

/*max is the value of E(10) where the difference (Q linear- Q logarithmic) reaches a maximum
this will be an E(10) value not on the extremes, neither too small nor too large*/

replace max=(ln(c2/(c4*2.3025*.0001))-2.3025*c3)/(2.3025*c4)	

local j=1
while `j'<=17{

/*first for low values of E(10)*/

local e=25
local delta=10
while (`delta'>=.0001 &(`e'>=20&`e'<max[`j'])) {
local f=(c1[`j']+c2[`j']*`e')-(.0001*exp(2.3025*(c3[`j']+c4[`j']*`e')))		/*function*/
local fprime=c2[`j']-2.3025*.0001*c4[`j']*exp(2.3025*(c3[`j']+c4[`j']*`e'))	/*derivative*/
local newe =-(`f'/`fprime') +`e'						/*Taylor series*/
local delta=abs(`e'-`newe')					
local e =-(`f'/`fprime') +`e'
}

replace eten1= `e' if _n==`j'

/*now for high values of E(10)*/

local e=max[`j']+10
local delta=10
while (`delta'>=.0001 &(`e'>=max[`j']&`e'<70)) {
local f=(c1[`j']+c2[`j']*`e')-(.0001*exp(2.3025*(c3[`j']+c4[`j']*`e')))		/*function*/
local fprime=c2[`j']-2.3025*.0001*c4[`j']*exp(2.3025*(c3[`j']+c4[`j']*`e'))	/*derivative*/
local newe =-(`f'/`fprime') +`e'						/*Taylor series*/
local delta=abs(`e'-`newe')					
local e =-(`f'/`fprime') +`e'
}
replace eten2= `e' if _n==`j'

local j=`j'+1
}

save CD_coefficients, replace

/*it will now add a last dummy record for age 80*/

preserve
drop _all
range age 80 80 1
replace age=80
gen c1=.
gen c2=.
gen c3=.
gen c4=.
gen max=.
gen eten1=.
gen eten2=.
save temp, replace
restore
append using temp
save CD_coefficients, replace
!del temp.dta

end
