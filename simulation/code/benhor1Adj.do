clear
program drop _all

/**************************/
/**************************/
/**************************/

program define benhor1_adj
set more 1


/*BENNETT-HORIOUCHI (1981) METHOD FOR COMPLETENESS

syntax is	bh1 zz uu vv

e.g.		bh1 1980 1990 ctry type   

xxxx:	country code
yy:	last two digit of the year the first census took place
zz:	last two digit of the year the second census took place
uu:	month the first census took place
vv:	month the second census took place
*/

bh1 `1' `2' `3'   
end	


/**************************/
/**************************/
/**************************/

program define bh1
set more off

*use X:\Palloni_Datasets\BOOKLA-DATA-2013\BookMortalityLA2013\simulations2017\SimulatedPopulations\SimulatedPopulationsAll_brass2
use "F:\Palloni_Datasets-12-12-2017\BOOKLA-DATA-2013--GP\BookMortalityLA2013\simulations2017\SimulatedPopulations\SimulatedPopulationsGroupAll_brass2.dta"

keep if ctry==`3' 

recode age 75/100=75
collapse (first) ctry teta1-comp3 family model scenario C (sum) pm1980-df1989, by(age)
sort ctry

*merge n:1 ctry using brass2T
drop if age==0

gen dm=0
gen df=0

sort ctry

local i=`1'
while `i'<=`2'-1 {
by ctry: replace dm=dm+dm`i'
by ctry: replace df=df+df`i'
local i=`i'+1
      	     }

/* accumulated population & mortality */

gen Pm`2'=sum(pm`2')
gen Pf`2'=sum(pf`2')

gen Pm`1'=sum(pm`1')
gen Pf`1'=sum(pf`1')

gen Dm=sum(dm)
gen Df=sum(df)

/* ADJUSTED population rates of growth */

gen rm`1'`2'=ln(pm`2'/pm`1')/(`2'-`1') + ln(C)/(`2'-`1')
gen rf`1'`2'=ln(pf`2'/pf`1')/(`2'-`1') + ln(C)/(`2'-`1')

/* Intercensal person-years lived at exact ages and accumulated person-years */

gen Nm=(pm`2'-pm`1')/rm`1'`2'
gen Nf=(pf`2'-pf`1')/rf`1'`2'

*gen Nm=(pm`2'-pm`1')/(ln(pm`2'/pm`1')/(`2'-`1'))
*gen Nf=(pf`2'-pf`1')/(ln(pf`2'/pf`1')/(`2'-`1'))

/* accumulated population & mortality from bottom to top */

gsort -age

gen PPm`2'=sum(pm`2')
gen PPf`2'=sum(pf`2')

gen PPm`1'=sum(pm`1')
gen PPf`1'=sum(pf`1')

gen DDm=sum(dm)
gen DDf=sum(df)

sort age

/* mortality rates */

gen double drfa=df/(`2'-`1')
gen double drma=dm/(`2'-`1')


/* propotion 30D20/20D40 to compute the life expectancy */

local pf=(df[3]+df[4]+df[5]+df[6]+df[7]+df[8]+df[9])/(df[9]+df[10]+df[11]+ df[12]+df[13]+df[14]+df[15]) 
local pm=(dm[3]+dm[4]+dm[5]+dm[6]+dm[7]+dm[8]+dm[9])/(dm[9]+dm[10]+dm[11]+ dm[12]+dm[13]+dm[14]+dm[15]) 

local E75f = 9.88496-3.71428*`pf'
local E75m = 9.88496-3.71428*`pm'

/* Population aged "a" to "a+5", ^N(a) */

gsort -age

gen Naf= df*(exp(rf`1'`2'*`E75f')-(((rf`1'`2'*`E75f')^2)/6)) if _n==1 
replace Naf= Naf[_n-1]*exp(5*rf`1'`2'[_n])+ df[_n]*exp(2.5*rf`1'`2'[_n]) if _n>=2 & _n<=16

gen Nam= dm*(exp(rm`1'`2'*`E75m')-(((rm`1'`2'*`E75m')^2)/6)) if _n==1 
replace Nam= Nam[_n-1]*exp(5*rm`1'`2'[_n])+ dm[_n]*exp(2.5*rm`1'`2'[_n]) if _n>=2 & _n<=16


sort age


/* Estimation of the Average person-years lived (exposure) ^N(a) */

gen Nef= 2.5*(Naf[_n] + Naf[_n+1]) if _n>=1 & _n<=15
replace Nef=0 if _n==16

gen Nem= 2.5*(Nam[_n] + Nam[_n+1]) if _n>=1 & _n<=15
replace Nem=0 if _n==16


/* Accumulated person-years lived to calculate ^N(10, x-5) and populations */

gen NNef= Nef[_n] + Nef[_n+1] if _n>=1 & _n<16   
gen NNem= Nem[_n] + Nem[_n+1] if _n>=1 & _n<16  

gen NNf= Nf[_n] + Nf[_n+1] if _n>=1 & _n<16   
gen NNm= Nm[_n] + Nm[_n+1] if _n>=1 & _n<16  


/* completeness ratio */


gen C5f=Nef/Nf if _n>1 & _n<16
gen C5m=Nem/Nm if _n>1 & _n<16

gen C75f=NNef/NNf if _n>1 & _n<15
gen C75m=NNem/NNm if _n>1 & _n<15

format C5f C75f %4.3f 

keep age ctry teta1 teta3 comp1 comp2 comp3 C5f C75f family model scenario
save benhor1_`3', replace
end


/**************************/
/**************************/
/**************************/

program define benhor

  local i=1
  while `i'<=63720 {
bh1 1980 1990 `i' 
  local i=`i'+1
              }
end
benhor

/**************************/
/**************************/
/**************************/

program define join

use benhor1_1
  local i=1
  while `i'<=63720 {
append using benhor1_`i'
  local i=`i'+1
		  }	

save benhor1T2_adj, replace
!erase benhor1_*.dta

end
join

