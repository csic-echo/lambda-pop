use CLEANfinallymatrixS-BH, clear

gen gen iadjcumsr2= 1/adjcumsr2

forval country=1/6300 {
twoway scatter iadjcumsr2 age if ctry==`country' 

graph save graphcumSR_`country', asis replace

}

