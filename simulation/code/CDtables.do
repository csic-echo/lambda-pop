program define CDtables

set more 1


/*this program takes values of ETEN (E(10)) as inputs and calculates 
a Coale Demeny life table using algorithm for females and MODEL WEST.
It outputs all functions of the life table but right now it is set up
to output only age and Lx values

The only modification with respect to the Coale and Demeny tables is that
to calculate individual values of lx from age 5 until age 75, I use the
separation factors for age groups 5-9 to 70-74 and assume that the 5Dx
are LINEAR within unit intervals but with a slope that is consistent with
the separation factors. In particular,

		1dx+k= A+B*k for k=0,1,2,3,4
		
		and 
		
		A=.2236*5Dx ; B=.0118*5Dx for  age group 5-9 (separator=2.25)
		A=.2094*5Dx ; B=.00471*5Dx for age groups 10-14....70-74
		
These changes enable us to calculate individual values of lx and Lx 
without using SPRAGUE.DO

To run the program one must feed a value of ETEN (input `1') and we must
provide a datafile containing the regression coefficients for CD Table for 
age groups 0,1-4, 5-9,....,75+. In addition, for each age group this data
file must contain the values of E(10) indicating the values of E(10) where
the linear and logarithmic regression lines intersect. This is done by the 
program PREPARE.DO. The file name is CD_coefficients.dta

This data file has variables age c1 c2 c3 c4, corresponding to the regression
coefficients of q on eten and log (base ten) of 10,000q on eten respectively. In 
addition it contains the values of E1 and E2 corresponding to the low and high 
E(10) at which the logarithmic and linear regression cross for that age

VERY VERY IMPORTANT: The data file must have a dummy record for age 80 to enable
us to calculate l(80) when we estimate the lx values. This dummy record will
have values of MISSING for  c1 through E2. 

The table is in the file CD_coefficients.dta This corresponds the input `2'

Syntax is:		 CDtables value CD_coefficients id

where:

"value" is the value of E(10)
"CD-Coefficients.dta"  is the file containing CD coefficients and the two intersection points (see
PREPARE.DO for an explanation)
"id" is ANY number to identify the file produced at the end

*/

use `2'

/*labels initial input*/

local eten=`1'

/*Q values for ages 0,1, 5....75*/

gen Q1=0
gen Q2=0
gen Q3=0
gen Q=0

replace Q1=c1+c2*`eten' 
replace Q2=(10^(c3+c4*`eten'))/10000	
replace Q3=.5*(Q1+Q2)	

replace Q=Q1						/*linear*/
replace Q= cond(`eten'<eten2&`eten'>eten1,Q3,Q) 	/*average of linear and logarithmic*/
replace Q= cond(`eten'>eten2, Q2, Q) 		 	/*logarithmic*/	

replace Q=1 if _n==18					/*only for completeness; it will not be used*/

/*calculates values of lx*/

gen lx=0
replace lx=100000 if _n==1			
replace lx=lx[_n-1]*(1-Q[_n-1]) if _n>1		/*we have values l(o), l(1), l(5),...l(80)*/ 

/*calculates values of Lx*/

local k01=.35
local k02=.050+3.0*Q[1]

local k11=1.361
local k12=1.524-1.625*Q[1]

local k3=2.25					
local k4=2.6					
local k5=2.5

gen Lx=0

replace Lx=cond(Q>=.100, `k01'*lx + (1-`k01')*lx[_n+1], `k02'*lx+(1-`k02')*lx[_n+1]) if _n==1
replace Lx=cond(Q>=.100, `k11'*lx + (4-`k11')*lx[_n+1], `k12'*lx+(1-`k12')*lx[_n+1]) if _n==2

replace Lx=`k3'*lx +(5-`k3')*lx[_n+1] if (_n==3)
replace Lx=`k4'*lx +(5-`k4')*lx[_n+1] if (_n>=4 & _n<17)
replace Lx=`k5'*lx +(5-`k5')*lx[_n+1] if ( _n==17)


/*we now need to do 3 things:

	create the values l(2),l(3), l(4) and L1, L2, L3 and L4
	create the values of l(85),l(90),l(95),l(100), l(105)
	
*/

save temp1, replace


			/*CREATING VALUES OF l(2)...L4*/

preserve

keep age Q lx Lx			/*keeps only age, Q, Lx values*/
keep if _n<=2				/*keeps values for age 0 and 1*/

range newage 0 5 6

			/*calculates l(x) anew*/
gen newlx=0
replace newlx=100000 if _n==1			/*this defines l(0)*/
replace newlx=newlx[1]*(1-Q[1]) if _n==2			/*this defines l(1)*/
replace newlx=newlx[2]*(1-Q[2]) if _n==6			/*this defines l(5)*/

			/*lx for x=  2,3 and 4 when Q[1]>=.100*/


replace newlx=.489*newlx[2]+(1-.489)*newlx[6] if (_n==3&Q[1]>=.100)	/*this defines l(2)*/	
replace newlx=.260*newlx[2]+(1-.260)*newlx[6] if (_n==4&Q[1]>=.100)	/*this defines l(3)*/
replace newlx=.112*newlx[2]+(1-.112)*newlx[6] if (_n==5&Q[1]>=.100)	/*this defines l(4)*/
			
			/*lx for x= 2,3 and 4 when Q[1]<.100*/
			
local aux1= .489+.656*(.100-Q[1])
local aux2= .260+.601*(.100-Q[1])
local aux3= .112+.370*(.100-Q[1])

replace newlx=`aux1'*lx[2]+(1-`aux1')*newlx[6] if (_n==3&Q[1]<.100)	/*this defines l(2)*/	
replace newlx=`aux2'*lx[2]+(1-`aux2')*newlx[6] if (_n==4&Q[1]<.100)	/*this defines l(3)*/
replace newlx=`aux3'*lx[2]+(1-`aux3')*newlx[6] if (_n==5&Q[1]<.100)	/*this defines l(4)*/

		/*calculates Lx for x=0,1,2,3,4.Except for L0, all others assume linearity; 
		see COALE AND DEMENY explanation in page 23, first column, second paragraph*/
			

gen biglx=0
replace biglx=Lx if _n==1
replace biglx=.5*(newlx[_n]+newlx[_n+1]) if (_n>1&_n<6)

rename age oldage
rename Lx oldLx 
rename lx oldlx
rename newage age
rename biglx Lx
rename newlx lx
keep oldage Q oldlx oldLx age lx Lx
drop if _n==6
order oldage Q oldlx oldLx age lx Lx
sort age
save temp2, replace	/*contains values of x lx and Lx for x=0,1,2,3,4*/

restore


			/*CREATING VALUES OF l(x) and Lx for x=85,86,104*/
preserve

keep age Q lx Lx				/*keeps only age, Q, Lx values*/
keep if _n>=17					/*keeps values for ages 75 and 80*/

range newage 75 100 26

			/*Cranks out the value of Gompertz slope*/


local mu105=.613+1.75*Q[1]		/*Q[1] refers to Q75!!*/
local mu775=(lx[1]-lx[2])/Lx[1]		/*lx[1] and lx[2] refer to l75,l80;Lx[1] refers to 5L75*/
local gompertz= ln(`mu105'/`mu775')/27.5
local mu80= `mu775'*exp(2.5*`gompertz')


	/*rearranges the values of lx and Lx to correspond to the age groups*/

gen newlx=0
gen newLx=0
replace newlx=lx[1] if _n==1		/*for age 75*/
replace newlx=lx[2] if _n==6		/*for age 80*/

rename age oldage
rename lx oldlx
rename Lx oldLx

/*
	
	IMPORTANT READ THIS:
	for age group 75-79 the values of lx are linear by definition; see CD, p.20
	BUT THIS CAUSES a problem since the mean time to death BEFORE age 75 is 2.6
	(see CD, p.20). If number of deaths within an interval are linear, assuming
	2.5 for 75-79 causes a discontinuity at age 75 in values of Lx and survival
	ratios. Thus,below there are two routines to construct lx within 75079.One
	assumes linearity as do CD. The other assumes that the value for the mean
	waiting time to death is 2.6 NOT 2.5 and treats this age group just as we
	treat below the age groups between 10 and 74
	
*/
	
	/*
	ALTERNATIVE I:assuming linearity: if you do not want to use it, comment this out
	*/


local t=2
while `t'<=5 {
replace newlx=newlx[1]-((`t'-1)/5)*(newlx[1]-newlx[6]) if _n==`t'
local t=`t'+1
}

local t=1
while `t'<=5 {
replace newLx=.5*(newlx[`t']+ newlx[`t'+1]) if _n==`t'
local t=`t'+1
}


/*	
	ALTENATIVE II; ASSUMING THAT KX=2.6 NOT 2.5 AND THAT ## of DEATHS
	IN THE AGE GROUP PROCEED LINEARLY
*/


/*
local D=(newlx[1]-newlx[6])
local C=(`D'/5)*.9529
local E=.00471*`D'
local t=1
while `t'<=5 {
local id=`t'+1			/*indicator for lx*/
local d`id'=(`C'+`E'*(`t'-1)) 	/*case where separator is 2.6*/
replace newlx=newlx[`id'-1]-`d`id'' if (_n==`id') 
local t=`t'+1
}

local t=1
while `t'<=5 {
replace newLx=.5*(newlx[`t']+ newlx[`t'+1]) if _n==`t'
local t=`t'+1
}

*/


/*for ages 81 to 100 we use single years values produced by Gompertz and starting at 80*/

local i=7
while `i'<=26 {
local r=(`i'-6)
replace newlx= newlx[6]*exp(-(`mu80'/`gompertz')*(exp(`gompertz'*`r')-1)) if _n==`i'
local i=`i'+1
}

/*we calculate Lx evaluating l(x) at x, x+.20, x+.40,...x+1 and then adding small areas
between these ages; for ages 80, 81....99*/

local t=6
while `t'<=25 {
local g=0
local suma`t'=0
	while `g'<=4 {
	local r=(`t'-6)+ (`g'+1)/5
	local s=(`t'-6)+ `g'/5
	local l1=newlx[6]*exp(-(`mu80'/`gompertz')*(exp(`gompertz'*`r')-1))
	local l2=newlx[6]*exp(-(`mu80'/`gompertz')*(exp(`gompertz'*`s')-1))
	local l3=(1/10)*(`l1'+`l2')
	local suma`t'=`suma`t''+`l3'
	local g=`g'+1
	}

replace newLx=`suma`t'' if _n==`t'
local t=`t'+1
}

/*for age 100 we calculate L100+ using values all the way up 150*/


local t=101
local sumatum=0
while `t'<=151 {
local g=0
local suma`t'=0
	while `g'<=4 {
	local r=(`t'-81)+ (`g'+1)/5
	local s=(`t'-81)+ `g'/5
	local r1=newlx[6]*exp(-(`mu80'/`gompertz')*(exp(`gompertz'*`r')-1))
	local r2=newlx[6]*exp(-(`mu80'/`gompertz')*(exp(`gompertz'*`s')-1))
	local r3=(1/10)*(`r1'+`r2')
	local suma`t'=`suma`t''+`r3'
	local g=`g'+1
	}

local sumatum=`suma`t'' +`sumatum' 
local t=`t'+1
}
replace newLx=`sumatum' if _n==26

rename newage age
rename newlx lx
rename newLx Lx
keep oldage Q oldlx oldLx age lx Lx
order oldage Q oldlx oldLx age lx Lx
sort age
save temp3, replace

restore

keep age Q lx Lx
order age Q lx Lx

/*produces individual values of lx and Lx for x=5,6,.....74*/

/*to do this we take serioulsy the separator values used by CD to generate the 5Lx values
from lx and l(x+5). We assume that 1dx descend linearly when separator is 2.25 or increase
linearly, when separator is 2.6. In both cases 1dx+k = A+B*k (k=0,1,2,3,4). It can be shown 
that A=5Dx*.2236 and that B=.0118*5Dx where 5Dx is the number of deaths in the age group*/

keep if (_n>=3&_n<=17)		/*keeps ages 5,10,15 ...75*/

range newage 5 75 71

/*rearranges to correspond to age groups*/


gen newlx=0
gen newLx=0

local j=1
while `j'<=15 {
	local a=(`j'-1)*5+1
	replace newlx=lx[`j']	 if _n==`a'
	local j=`j'+1
}


/*does values by single years of age*/

local b=1
while `b'<=66 {
	local a=`b'+5
	local D=(newlx[`b']-newlx[`a'])
	local A=(`D'/5)*1.118 
	local B=-.0118*`D'
	local C=(`D'/5)*.9529
	local E=.00471*`D'

	local t=0
	while `t'<=4 {
	local id=`b'+`t'+1		/*indicator for lx*/
	local d1`id'=(`A'+`B'*`t') 	/*case where separator is 2.25*/
	local d2`id'=(`C'+`E'*`t') 	/*case where separator is 2.6*/
	
	replace newlx=newlx[`id'-1]-`d1`id'' if (_n==`id'&`b'==1) 
	replace newlx=newlx[`id'-1]-`d2`id'' if (_n==`id'&`b'>1) 
	
	local t=`t'+1
	}
local b=`b'+5
}

local f=1
while `f'<=70 {
replace newLx=.5*(newlx[`f']+newlx[`f'+1]) if _n==`f'
local f=`f'+1
}

rename age oldage
rename newage age
rename lx oldlx
rename Lx oldLx
rename newlx lx
rename newLx Lx

keep age Q lx Lx oldage oldlx oldLx
drop if _n==_N				/*drop last record for age 75*/
order oldage Q oldlx oldLx age lx Lx
sort age
save temp4, replace

drop _all

use temp2
append using temp4
append using temp3

keep oldage Q oldlx oldLx age lx Lx
sort age

/*calculates survival ratios and performs an adjustment to have in the last age (100) 
the probability of survivng from age 99+ to age 100+*/

gen srx=0
replace srx=Lx[1]/100000 if _n==1
replace srx=Lx/Lx[_n-1] if (_n>1 & _n<=100)
summarize Lx if _n>=100			/*this should be L99 plus L100+*/
local sum1=r(N)*r(mean)
summarize Lx if _n>=101			/*this should be L100+*/
local sum2=r(N)*r(mean)
replace srx=`sum2'/`sum1' if _n==101	/*prob of surviving from 99+ to 100+*/
drop if _n>101
sort age
save table_`3'.dta, replace
end
