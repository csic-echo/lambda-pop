clear
program drop _all

program define brass1
set more 1


/*BRASS METHOD FOR COMPLETENESS OF DEATH RECORDS.

this program calculates the completeness of death registration using the Brass (1975) method based on stability using only one census. The other assumptions of the method are a closed population and the completeness of death registration is constant across ages.
Two answers are computed:
a. One assuming constant growth rate across ages
b. The other assuming different age-specific growth rates
The program is invoked with the following syntax:

			brass1 xxxx ww yy

e.g.			brass1 1980 1990 125

where x is the country code, yy represent (last 2 digits) years of census 
and ww represent (last 2 digits of the previous census)*/


br1 `1' `2' `3' 

end

/***************************************************/

program define br1
set more off

*use X:\Palloni_Datasets\BOOKLA-DATA-2013\BookMortalityLA2013\simulations2017\SimulatedPopulations\SimulatedPopulationsAll
use "F:\Palloni_Datasets-12-12-2017\BOOKLA-DATA-2013--GP\BookMortalityLA2013\simulations2017\SimulatedPopulations\SimulatedPopulationsGroupAll.dta"

keep if ctry==`3' 

recode age 75/100=75
collapse (first) ctry teta1-comp3 family model scenario (sum) pm1980-df1989, by(age)


gen dmt=0			/*dmt are males deaths and dft females deaths, yearly*/
gen dft=0
gen yr2=`1'

sort ctry
local i=`1'
while `i'<=`1'+ 9 {
by ctry: replace dmt=dmt+dm`i'
by ctry: replace dft=dft+df`i'
local i=`i'+1
		   }		

/*3-year average of deaths was used to smooth deaths.*/	

gen dm=dmt/10
gen df=dft/10

/* accumulated population & mortality from bottom to top */

gsort -age

gen Pm`2'=sum(pm`2')
gen Pf`2'=sum(pf`2')

gen Pm`1'=sum(pm`1')
gen Pf`1'=sum(pf`1')

gen Dm=sum(dm)
gen Df=sum(df)

sort age

/* calculates cumulated pop N(x), birth rate b(x), and death rate d(x) */

gen Nmx=(pm`1'[_n-1]+pm`2'[_n])/10
gen Nfx=(pf`1'[_n-1]+pf`2'[_n])/10

gen bmx=Nmx/Pm`2'
gen bfx=Nfx/Pf`2'

gen dmx=Dm/Pm`2'
gen dfx=Df/Pf`2'

/* rate of growth for population aged 0+, 5+, ... */

gen rm_age=(ln(Pm`2'/Pm`1'))/(`2'-`1') 
gen rf_age=(ln(Pf`2'/Pf`1'))/(`2'-`1')  

/* Corrected birth rates:  b(x)-growthrates(x+) */

gen newbmx=bmx-rm_age
gen newbfx=bfx-rf_age

disp ""

disp "Version 1:  Assuming constant growth rates across ages"
disp ""

/* calculates slope using OLS and different exclusions of extreme ages */

drop if (age==0)

quietly regress bmx dmx if age<75   /*Last age group is excluded */
disp "Male slope with all data"
matrix A=e(b)
disp el(A,1,2)
disp el(A,1,1) 
gen C75m1=1/el(A,1,1)

quietly regress bmx dmx  if age<70
disp "Male slope without age>=70"
matrix A=e(b)
disp el(A,1,2)
disp el(A,1,1)
gen C70m1=1/el(A,1,1)


quietly regress bmx dmx if age<65
disp "Male slope without age>=65"
matrix A=e(b)
disp el(A,1,2)
disp el(A,1,1)
gen C65m1=1/el(A,1,1)


disp ""

quietly regress bfx dfx if age<75  /*Last group is excluded */
disp "Female slope with all data"
matrix A=e(b)
disp el(A,1,2)
disp el(A,1,1)
gen C75f1=1/el(A,1,1)

quietly regress bfx dfx if age<70
disp "Female slope without age>=70"
matrix A=e(b)
disp el(A,1,2)
disp el(A,1,1)
gen C70f1=1/el(A,1,1)

quietly regress bfx dfx if age<65
disp "Female slope without age>=65"
matrix A=e(b)
disp el(A,1,2)
disp el(A,1,1)
gen C65f1=1/el(A,1,1)

disp ""
disp ""


disp "Version 2:  Assuming different growth rates across ages"
disp ""

/* calculates slope using OLS and different exclusions of extreme ages */

drop if (age==0)
quietly regress newbmx dmx if age<75   /*Last age group is excluded*/
disp "Male slope with all data"
matrix A=e(b)
disp el(A,1,2)
disp el(A,1,1)
gen C75m=1/el(A,1,1)

quietly regress newbmx dmx  if age<70
disp "Male slope without age>=70"
matrix A=e(b)
disp el(A,1,2)
disp el(A,1,1)
gen C70m=1/el(A,1,1)


quietly regress newbmx dmx if age<65
disp "Male slope without age>=65"
matrix A=e(b)
disp el(A,1,2)
disp el(A,1,1)
gen C65m=1/el(A,1,1)


quietly regress newbfx dfx if age<75   /*Last age group is exclude because it is an open group */
disp "Female slope with all data"
matrix A=e(b)
disp el(A,1,2)
disp el(A,1,1)
gen C75f=1/el(A,1,1)


quietly regress newbfx dfx if age<70
disp "Female slope without age>=70"
matrix A=e(b)
disp el(A,1,2)
disp el(A,1,1)
gen C70f=1/el(A,1,1)


quietly regress newbfx dfx if age<65
disp "Female slope without age>=65"
matrix A=e(b)
disp el(A,1,2)
disp el(A,1,1)
gen C65f=1/el(A,1,1)

drop if _n>=2 & _n<=15

format C75f %4.3f

keep ctry age teta1 teta3 comp1 comp2 comp3 C65f family model scenario 
save brass1_`3', replace
end


/**************************/
/**************************/
/**************************/

program define brass

  local i=1 
  while `i'<=63720  {
br1 1980 1990 `i' 
  local i=`i'+1
		  }	

end
brass

/**************************/
/**************************/
/**************************/

program define join

set more off
use brass1_1
local i=2
  while `i'<=63720  {
append using brass1_`i'
  local i=`i'+1
		  }	

save brass1T, replace

!erase brass1_*.dta

end
join
