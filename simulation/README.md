# Simulation of populations with defective completeness and age misstatment

This directory contains scripts to produce simulated stable populations and to generate estimates of relative completeness, parameters of older age misstatements and adjustments for completeness and age misstatement

The directory includes an excel file describing the sequence of steps and associated scripts to simulate, make diagnostics about completeness and age misreporting, and adjust the raw data subject to errors.

All other files are scripts whose function is identified in the excel file.