# Estimation of older-adult mortality from information distorted by systematic age misreporting 

Code to reproduce analysis of the paper [Estimation of older-adult mortality from information distorted by systematic age misreporting](https://doi.org/10.1080/00324728.2021.1918752) by A. Palloni, H. Beltran-Sanchez, and G. Pinto.
- Guatemala illustration (`guatemala`)
- Simulation of populations with defective completeness and age misstatement (`simulation`)